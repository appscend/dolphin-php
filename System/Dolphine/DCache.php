<?php
class DCache
{
  private static $_memcache;
  private static $_instance;

  public static function init()
    {
        if (self::$_instance === null)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // protected -> implement this;
    public function __construct()
    {
        try {
            self::$_memcache = new \Memcache;
            self::$_memcache->connect('localhost', 11211);
        } catch (Exception $e) {
            \System\Dolphine\Registry::setSetting('master_cache', 'off');
        }
    }

    public static function getCachedData()
    {
      $key = self::generateCacheKey();
      return self::$_memcache->get($key);
    }

    public static function setCachedData( $data, $_time_cache = null )
    {
      if( !isset($_time_cache) ) $_time_cache = 900;
      $key = self::generateCacheKey();
      self::$_memcache->set($key, $data, false, $_time_cache);
    }

    public static function storeObject( $data, $gzip = false, $_time_cache = null )
    {
        if(!isset($_time_cache)) $_time_cache = 900;
        $key = self::generateCacheKey();
        $_data = array(
            'source' => $data,
            'gzip' => $gzip,
        );
        $_data = base64_encode(json_encode($_data));
        self::$_memcache->set($key, $_data, false, $_time_cache);
    }

    public static function retrieveObject()
    {
        $_data = false;
        $key = self::generateCacheKey();
        $data = self::$_memcache->get($key);
        if( isset($data) ){
            $_data = json_decode(base64_decode($data), true);
        }
        return $_data;
    }

    public static function removeCachedData()
    {
      $key = self::generateCacheKey();
      if(self::$_memcache->get($key)) {
          self::$_memcache->delete($key);
        }
    }

    public static function flushCache()
    {
      self::$_memcache->flush();
    }

  public static function generateCacheKey()
  {
      $key = 'key';

      if(isset($_SERVER['PATH_INFO'])){
        $key = explode('/', $_SERVER['PATH_INFO']);
      } else {
            DolphinError::writeError( '$_SERVER["PATH_INFO"] is unavailable' );
        }

        $vars = $_REQUEST;
      if(isset($vars['timezone'])) unset($vars['timezone']);
      if(isset($vars['timestamp'])) unset($vars['timestamp']);
      if(isset($vars['udid'])) unset($vars['udid']);
      if(isset($vars['carrier'])) unset($vars['carrier']);
        if(isset($vars['imei'])) unset($vars['imei']);
        if(isset($vars['locale'])) unset($vars['locale']);
        if(isset($vars['os'])) unset($vars['os']);
        if(isset($vars['device'])){
          if(strtolower(substr( $vars['device'], 0, 7 )) === "iphone5" || strtolower(substr( $vars['device'], 0, 5 )) === "ipod5" ){
         $vars['device'] = 'device5';
          }else{
         $vars['device'] = 'device';
          }
        }

      $vars = print_r($vars, true);

      $hash = hash('md5', $vars);

      if(!isset($key[2])) $key[2] = 'index';

      return ucfirst($key[1].'_'.$key[2].'_'.$hash);

  }

}
