<?php
class listCellConfig
{
	public $_params;

	public function __construct()
	{
		self::init();
	}

	private function init()
	{
		$this->_params = array();
	}

	// Override cell properties
	// 
	public function overrideConfigCell( $prop, $value, $valuPad = null )
	{
		$this->setProperty( $prop, $value, $valuPad);
	}

	public function setColorCell( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'cc', $value, $valuePad);
	}

	public function setSelectedColorCell( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'scc', $value, $valuePad);
	}

	public function setImageCell( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'ci', $value, $valuePad);
	}

	public function setSelectedImageCell( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'sci', $value, $valuePad);
	}

	public function setAlternativeColorCell( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'acc', $value, $valuePad);
	}

	public function setAlternativeImageCell( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'aci', $value, $valuePad);
	}

	public function setCellLeftPadding( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'clp', $value, $valuePad);
	}

	public function setCellRightPadding( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'crp', $value, $valuePad);
	}

	public function setCellLeftMargin( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'clm', $value, $valuePad);
	}

	public function setCellRightMargin( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'crm', $value, $valuePad);
	}

	public function setSeparatorColor( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'csc', $value, $valuePad );
	}

	public function setCellHeight( $value, $valuePad = null )
	{
		$this->overrideConfigCell( 'ch', $value, $valuePad );
	}

	public function setHeight( $value, $valuePad = null ) 
	{
		$this->setCellHeight( $value, $valuePad );
	}	

	public function setAccessoryEnabled( $val = true )
	{
		if(!$val){
			$this->overrideConfigCell( 'aisw', 0, null);
			$this->overrideConfigCell( 'aish', 0, null);
			$this->overrideConfigCell( 'ai', 0, null);
		}
	}
	public function setKeepCellSelected( $val = true )
	{
		$this->overrideConfigCell( 'ks', $val, null);
	}


	// End Override cell properties
	//

	public function setProperty( $param, $value, $valuePad = null  )
	{
		if(isset($param) && isset($value))
		{
			$this->_params[$param] = viewHelper::boolToLogic($value);
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($value)) {
			$this->_params[$param] = $value;
			return $value;	
		}
	}

	private function addSingleLevel( $params , $where )
	{
		foreach ($params as $key => $value) {
            if (!is_null($value)) {
            	if(is_object($value))
            	{
            		array_push($this->$where, $value);
            	}
            }
        }
	}

}