<?php
class cellDetailArea extends cellElement
{
	public function __construct($title = null)
	{
		parent::__construct($title, 'dv');
	}

	public function setTopPadding( $val, $valPad = null )
	{
		$this->setProperty( 'tp', $val, $valPad, $this->_prefix );
	}

	public function setSidePadding( $val, $valPad = null )
	{
		$this->setProperty( 'sp', $val, $valPad, $this->_prefix );
	}

	public function setImageTopPadding( $val, $valPad = null )
	{
		$this->setProperty( 'itp', $val, $valPad, $this->_prefix );
	}

}