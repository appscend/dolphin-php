<?php
class listSection
{
	public $_params;

	public function __construct( $title = null )
	{
		self::init();
	}

	private function init()
	{
		$this->_params = array();
		$this->_params['cfg'] = array();
		$this->_params['es'] = array();
	}

	public function addConfig( $param, $value , $valuePad = null )
	{
		if(isset($param) && isset($value))
		{
			if($param != 'es'){
				$this->_params['cfg'][$param] = viewHelper::boolToLogic($value);
				if(isset($valuePad))
				{
					$param  = "pad".$param;
					$this->_params['cfg'][$param] = viewHelper::boolToLogic($valuePad);
				}
			}
		}
	}

	public function setName( $val, $valPad = null )
	{
		$this->addConfig('sn', $val, $valPad);
	}

	public function setIndex( $val , $display_index = true )
	{
		$this->addConfig('ssc', $val, null );
	}

	public function setNameHeight( $val, $valPad = null )
	{
		$this->addConfig('sh', $val, $valPad);
	}

	public function setNameFont()
	{
		$arg = func_get_args();
		$no_args = sizeof($arg);
		if($no_args > 0) $this->addConfig('sf', $arg[0], null );
		if($no_args > 1) $this->setNameSize( $arg[1], null );
		if($no_args > 2) $this->setNameColor( $arg[2], null );
	}

	public function setNameSize( $val, $valPad = null)
	{
		$this->addConfig('ss', $val, $valPad);
	}

	public function setNameColor( $color, $alpha = null )
	{
		$this->addConfig('sc', viewHelper::color($color, $alpha) );
	}

	public function setNameAlignment( $val, $valPad = null )
	{
		$a = self::checkIfAlignment($val);
		$b = self::checkIfAlignment($valPad);
		$this->addConfig('sa', $a, $b);
	}

	public function setNameTopPadding( $val, $valPad = null )
	{
		$this->addConfig('stp', $val, $valPad);
	}

	public function setBackgroundColor( $val, $valPad = null )
	{
		$this->addConfig('sbc', $val, $valPad);
	}

	public function addCell()
	{
		$cell = array();
		foreach (func_get_args() as $key => $obj) {
			if(isset($obj->_params)){
				$cell = array_merge( $cell, $obj->_params);
			}	
		}
		$e = array( 'e' => $cell );
		array_push( $this->_params['es'], $e );
	}

	public function createCell()
	{
		$cell = array();
		foreach (func_get_args() as $key => $obj) {
			if(isset($obj->_params)){
				$cell = array_merge( $cell, $obj->_params);
			}	
		}
		$e = array( 'e' => $cell );
		array_push( $this->_params['es'], $e );
	}

	public function setProperty( $param, $value, $valuePad = null  )
	{
		if(isset($param) && isset($value))
		{
			$this->_params['cfg'][$param] = viewHelper::boolToLogic($value);
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params['cfg'][$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params['cfg'][$param]))
		{
			unset($this->_params['cfg'][$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params['cfg'][$param]))
		{
			return $this->_params['cfg'][$param];
		} elseif(isset($value)) {
			$this->_params['cfg'][$param] = $value;
			return $value;	
		}
	}

	private function addSingleLevel( $params , $where )
	{
		foreach ($params as $key => $value) {
            if (!is_null($value)) {
            	if(is_object($value))
            	{
            		array_push($this->$where, $value);
            	}
            }
        }
	}

	private function checkIfAlignment( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'r', 'c', 'l', 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					return 'r';
					break;
				case 'center' :
					return 'c';
					break;
				case 'left' :
					return 'l';
					break;
				default :
					return $val;
					break;
			}
		} else {
			return null;
		}
	}
	
}