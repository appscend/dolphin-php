<?php
class element
{
	public $_params;
	public function __construct( $title = null )
	{
		self::init();
		if(isset($title))
		{
			self::setTitle( $title );
		}
	}

	private function init()
	{
		$this->_params = array();
	}

	public function showIfPurchaseMade( $prod , $prodPad = null )
	{
		$this->setProperty('prod', $prod, $prodPad );
	}

	public function showIfPurchaseNotMade( $nprod, $nprodPad = null )
	{
		$this->setProperty('nprod', $nprod, $nprodPad );
	}

	public function coords( $x, $y, $w, $h, $mode = null )
	{
		$te = '';
		if(isset($mode))
		{
			switch($mode)
			{
				case 'landscape' :
					$te = 'l';
					break;
				case 'l' :
					$te = 'l';
					break;
				case 't' :
					$te = 'pad';
					break;
				case 'tl' :
					$te = 'padl';
					break;
				case 'tablet':
					$te = 'pad';
					break ;
				case 'tablet portrait':
					$te = 'pad';
					break;
				case 'pad' :
					$te = 'pad';
					break;
				case 'padl' :
					$te = 'padl';
					break;
				case 'tablet landscape':
					$te = 'padl';
					break;
				default :
					$te = '';
					break;
			}
		}
		$this->_params[ $te.'x' ] = $x;
		$this->_params[ $te.'y' ] = $y;
		$this->_params[ $te.'w' ] = $w;
		$this->_params[ $te.'h' ] = $h;
	}

	public function setWidth( $val, $mode = '' )
	{
		$this->_params[ $mode.'w' ] = $val;
	}
	public function setHeight( $val, $mode = '' )
	{
		$this->_params[ $mode.'h' ] = $val;
	}

	public function setX( $val, $mode = '' )
	{
		$this->_params[ $mode.'x' ] = $val;
	}

	public function setY( $val, $mode = '' )
	{
		$this->_params[ $mode.'y' ] = $val;
	}

	public function setAvi( $val )
	{
		$this->setProperty('avi', $val);
	}

	public function setProperty( $param, $value, $valuePad = null  )
	{
		if(isset($param) && isset($value))
		{
			if(isset($value))
			{
				$this->_params[$param] = viewHelper::boolToLogic($value);
			}
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($value)) {
			$this->_params[$param] = $value;
			return $value;
		}
	}

	public function setTargetViewType( $val, $valPad = null )
	{
		self::setProperty( 'vt', $val, $valPad );
	}

	public function setTargetView( $val, $type = null )
	{
		self::setProperty( 'px', $val );
		self::setProperty( 'vt', $type );
	}

	public function setDataForTargetView( $val, $valPad = null )
	{
		if(is_array($val) || is_object($val))
		{
			$val = json_encode($val);
		}
		if(is_array($valPad) || is_object($valPad))
		{
			$valPad = json_encode($valPad);
		}
		self::setProperty( 'data', $val, $valPad );
	}

	public function setNavigationEnabled( $val = true )
	{
		if(is_bool($val))
		{
			$rtt =  ($val ? 'yes' : 'no');
			$this->setProperty('nav', $rtt);
		} else {
			$this->setProperty('nav', 'yes');
			$this->setProperty('navz', $val);
		}
	}

	public function setAlpha()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if( $no_args > 0 ) $this->setProperty( 'alpha', self::checkAlpha($args[0]) );
		if( $no_args > 1 ) $this->setProperty( 'lalpha', self::checkAlpha($args[1]) );
		if( $no_args > 2 ) $this->setProperty( 'padalpha', self::checkAlpha($args[2]) );
		if( $no_args > 3 ) $this->setProperty( 'padlalpha', self::checkAlpha($args[3]) );
	}

	private function checkAlpha( $val )
	{
		if(is_numeric($val))
		{
			if($val >= 0 && $val <= 1)
			{
				return $val;
			} else {
				return 1;
			}
		} else return 1;
	}

	public function setDraggable( $val, $valPad = null )
	{
		$this->setProperty('drag', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setDragHorizontaly( $val, $valPad = null )
	{
		$this->setProperty('dragx', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setDragVerticaly( $val, $valPad = null )
	{
		$this->setProperty('dragy', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setDragSpaceHorizontaly( $min, $max, $minP = null, $maxP = null )
	{
		if( is_int($min) && is_int($max) )
		{
			$this->setProperty('minx', $min, $minP);
			$this->setProperty('maxx', $max, $maxP);
		}
	}
	public function setDragSpaceVerticaly( $min, $max, $minP = null, $maxP = null )
	{
		if( is_int($min) && is_int($max) )
		{
			$this->setProperty('miny', $min, $minP);
			$this->setProperty('maxy', $max, $maxP);
		}
	}

	public function setHorizontalSnapCoords()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if(isset($args[0])) {
			$mode = $args[0];
		} else {
			$mode = null;
		}
		$te = 'snap';
		if(isset($mode) && !is_int($mode))
		{
			switch($mode)
			{
				case 'landscape' :
					$te = 'snapl';
					break;
				case 'l' :
					$te = 'snapl';
					break;
				case 't' :
					$te = 'padsnap';
					break;
				case 'tl' :
					$te = 'padsnapl';
					break;
				case 'tablet':
					$te = 'padsnap';
					break ;
				case 'tablet portrait':
					$te = 'padsnap';
					break;
				case 'pad' :
					$te = 'padsnap';
					break;
				case 'padl' :
					$te = 'padsnapl';
					break;
				case 'tablet landscape':
					$te = 'padsnapl';
					break;
				default :
					$te = 'snap';
					break;
			}
		}
		if(is_int($mode)){
			$this->_params[ $te.'x' ] = $mode;
		} else {
			$this->_params[ $te.'x' ] = '';
		}
		for($i = 1; $i < $no_args; $i += 1)
		{
			$this->_params[ $te.'x' ] .= '::'.$args[$i];
		}
	}

	public function setVerticalSnapCoords( )
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if(isset($args[0])) {
			$mode = $args[0];
		} else {
			$mode = null;
		}
		$te = 'snap';
		if(isset($mode) && !is_int($mode))
		{
			switch($mode)
			{
				case 'landscape' :
					$te = 'snapl';
					break;
				case 'l' :
					$te = 'snapl';
					break;
				case 't' :
					$te = 'padsnap';
					break;
				case 'tl' :
					$te = 'padsnapl';
					break;
				case 'tablet':
					$te = 'padsnap';
					break ;
				case 'tablet portrait':
					$te = 'padsnap';
					break;
				case 'pad' :
					$te = 'padsnap';
					break;
				case 'padl' :
					$te = 'padsnapl';
					break;
				case 'tablet landscape':
					$te = 'padsnapl';
					break;
				default :
					$te = 'snap';
					break;
			}
		}
		if(is_int($mode)){
			$this->_params[ $te.'y' ] = $mode;
		} else {
			$this->_params[ $te.'y' ] = '';
		}
		for($i = 1; $i < $no_args; $i += 1)
		{
			$this->_params[ $te.'y' ] .= '::'.$args[$i];
		}
	}

	public function setSnapDuration ( $val, $valPad = null )
	{
		$this->setProperty('snapd', $val, $valPad );
	}

	public function setZoomEnabled( $val, $valPad = null )
	{
		$this->setProperty('zoom', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setRotateEnabled( $val, $valPad = null )
	{
		$this->setProperty('rotate', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setTouchDisabled( $val, $valPad = null )
	{
		$this->setProperty('td', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setCornerRadius( $val, $valPad = null )
	{
		$this->setProperty('cr', $val, $valPad );
	}

	public function setClipEnabled( $val, $valPad = null )
	{
		$this->setProperty('clip', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setHideOnScreenShot( $val, $valPad = null )
	{
		$this->setProperty('hs', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	///////////////////////////////////


	public function setShadowEnabled( $val, $valPad = null )
	{
		$this->setProperty('shadow', viewHelper::boolToLogic($val), viewHelper::boolToLogic($valPad));
	}

	public function setShadowRadius( $val, $valPad = null )
	{
		$this->setProperty('shadowr', $val, $valPad );
	}

	public function setShadowColor( $val, $valPad = null )
	{
		$this->setProperty('shadowc', $val, $valPad = null );
	}

	public function setShadowAlpha( $val, $valPad = null )
	{
		$this->setProperty('shadowa', $val, $valPad = null );
	}

	public function setShadowXOffset( $val, $valPad = null )
	{
		$this->setProperty('shadowx', $val, $valPad);
	}

	public function setShadowYOffset( $val, $valPad = null )
	{
		$this->setProperty('shadowy', $val, $valPad);
	}

	///////////////////////////////////

	public function setBorderEnabled( $val = true, $valPad = null )
	{
		$this->setProperty('border', $val, $valPad);
	}

	public function setBorder()
	{
		$this->setBorderEnabled();
		$args = func_get_args();
		if(isset($args[0])) $this->setBorderSize( $args[0] );
		if(isset($args[1])) $this->setBorderColor( $args[1] );
	}

	public function setBorderColor( $val, $valPad = null )
	{
		$this->setProperty('borderc', $val, $valPad);
	}

	public function setBorderSize( $val, $valPad = null )
	{
		$this->setProperty('borders', $val, $valPad);
	}

	///////////////////////////////////

	public function setLocation( $value, $valuePad = null )
	{
		$val = viewHelper::boolToLogic($val);
		$valPad = viewHelper::boolToLogic($valPad);
		$this->setProperty('loc', $val, $valPad);
	}

	public function setLocationAccuray( $val, $valPad = null )
	{
		if(is_int($val)) {
			if(is_int($valPad)){
				$this->setProperty('loca', $val, $valPad);
			} else {
				$this->setProperty('loca', $val, null);
			}
		}
	}

	public function setId( $id, $padId = null )
	{
		$this->setProperty( 'itm', $id , $padId );
	}

	public function setTitle( $title , $titlePad = null)
	{
		$this->setProperty( 'n', $title , $titlePad );
	}

	public function setAlternativeTitle( $title, $titlePad = null )
	{
		$this->setProperty( 'an', $title, $titlePad = null );
	}

	public function setText( $text , $textPad = null )
	{
		$this->setProperty('n', $text, $textPad );
	}

	public function setAlternativeText( $text , $textPad = null )
	{
		$this->setProperty('an', $text, $textPad );
	}

	public function setImage( $image, $orientation = null )
	{
		switch ($orientation) {
			case 'l':
				$this->setProperty( 'li', $image );
				break;
			case 'pad':
				$this->setProperty( 'padi', $image );
				break;
			case 'padl':
				$this->setProperty( 'padli', $image );
				break;
			default:
				$this->setProperty( 'i', $image );
				break;
		}
	}

	public function setAlternativeImage( $image, $orientation = null )
	{
		switch ($orientation) {
			case 'l':
				$this->setProperty( 'lai', $image );
				break;
			case 'pad':
				$this->setProperty( 'padai', $image );
				break;
			case 'padl':
				$this->setProperty( 'padlai', $image );
				break;
			default:
				$this->setProperty( 'ai', $image );
				break;
		}
	}

	public function setImageLeftCap( $value, $valuePad = null )
	{
		$this->setProperty('ilc', $value, $valuePad );
	}

	public function setImageTopCap( $value, $valuePad = null )
	{
		$this->setProperty('itc', $value, $valuePad );
	}

	public function setFAVcat( $value, $valuePad = null )
	{
		$this->setProperty('afavcat', $value, $valuePad );
	}

	public function setFAVitm( $value, $valuePad = null )
	{
		$this->setProperty('afavitm', $value, $valuePad );
	}

	public function setFAVkey( $value, $valuePad = null )
	{
		$this->setProperty('afavkey', $value, $valuePad );
	}

	public function setNoLines( $val, $valPad = null )
	{
		$this->setProperty( 'nl', $val, $valPad );
	}

	public function setFontSize( $val, $valPad = null )
	{
		$this->setProperty( 'ns', $val, $valPad );
	}

	public function setFontColor( $val, $valPad = null )
	{
		$this->setProperty( 'nc', $val, $valPad );
	}

	public function setSlectedFontColor( $val, $valPad = null )
	{
		$this->setProperty( 'snc', $val, $valPad );
	}

	public function setTitleAlignment( $val, $valPad = null )
	{
		$a = self::checkIfAlignment( $val );
		$b = self::checkIfAlignment( $valPad );
		$this->setProperty( 'na', $a, $b );
	}

	public function setTextAlignment( $val, $valPad = null )
	{
		$a = self::checkIfAlignment( $val );
		$b = self::checkIfAlignment( $valPad );
		$this->setProperty( 'na', $a, $b );
	}

	public function setBackgroundColor( $val, $valPad = null )
	{
		$this->setProperty( 'bc', $val, $valPad );
	}

	public function setSelectedBackgroundColor( $val, $valPad = null )
	{
		$this->setProperty( 'sbc', $val, $valPad );
	}

	public function setImageMode( $icm , $icmPad = null )
	{
		$icm = self::checkIfIcm( $icm );
		$icmPad = self::checkIfIcm( $icmPad );
		$this->setProperty('icm', $icm, $icmPad );
	}

	public function setPadding()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if( $no_args == 1 )
		{
			$this->setProperty( 'np', $args[0], null );
		}
		if( $no_args > 1 ) $this->setProperty( 'np', $args[0].'::'.$args[1], null );
		if( $no_args > 2 ) $this->setProperty( 'np', $args[0].'::'.$args[1], null );
		if( $no_args > 3 ) $this->setProperty( 'np', $args[0].'::'.$args[1].'::'.$args[2].'::'.$args[3], null );
	}

	public function setAutoWidth( $val, $valPad = null )
	{
		$val = viewHelper::boolToLogic($val);
		$valPad = viewHelper::boolToLogic($valPad);
		$this->setProperty('aw', $val, $valPad);
	}

	public function setAutoHeight( $val, $valPad = null )
	{
		$val = viewHelper::boolToLogic($val);
		$valPad = viewHelper::boolToLogic($valPad);
		$this->setProperty('ah', $val, $valPad);
	}

	public function setPositionId ( $val, $valPad = null )
	{
		$this->setProperty( 'ahid', $val, $valPad );
	}

	public function setAutoPosition( $val, $valPad = null )
	{
		$this->setProperty( 'ahy', $val, $valPad );
	}

	public function setAutoPositionBy( $val, $valPad = null )
	{
		$this->setProperty( 'ahyrel', $val, $valPad );
	}

	public function setAutoPositionOffset()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if( $no_args > 0 ) $this->setProperty( 'yof', $args[0], null );
		if( $no_args > 1 ) $this->setProperty( 'lyof', $args[1], null );
		if( $no_args > 2 ) $this->setProperty( 'padyof', $args[2], null );
		if( $no_args > 3 ) $this->setProperty( 'padlyof', $args[3], null );
	}

	public function setClass( $val, $valPad = null )
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		$val = '';
		$i = 0;
		foreach ($args as $key => $value) {
			$i += 1;
			$val .= $value.'::';
			if($i == $args) $val .= $value;
		}
		$this->setProperty('key', $val, $valPad );
	}

	public function setKeyId( $val, $valPad = null )
	{
		$this->setProperty('key', $val, $valPad );
	}

	public function setFont()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if( $no_args > 0 ) $this->setProperty( 'nf', $args[0], $args[0] );
		if( $no_args > 1 ) $this->setProperty( 'ns', $args[1], $args[1] );
		if( $no_args > 2 ) $this->setProperty( 'nc', $args[2], $args[2] );
		if( $no_args > 3 ) $this->setProperty( 'snc', $args[3], $args[3]);
	}

	public function setImageWidth( $val, $valPad = null )
	{
		$this->setProperty( 'isw', $val, $valPad );
	}

	public function setImageHeight($val, $valPad = null)
	{
		$this->setProperty( 'ish', $val, $valPad );
	}

	public function setAction( $obj = null , $prefix = null )
	{
		if(!isset($prefix)) $prefix = '';
		if(isset($obj->_params))
		foreach( $obj->_params as $key=>$value ){
			$this->_params[$prefix.$key] = $value;
		}
	}

	public function addAction( $obj, $prefix = null )
	{
		$this->setAction($obj, $prefix);
	}

	public function setAlternativeAction( $obj )
	{
		$this->setAction( $obj, 'a' );
	}

	public function addAlternativeAction( $obj )
	{
		$this->setAction( $obj, 'a' );
	}

	public function addDoubleTapAction( $obj )
	{
		$this->setAction($obj, 'd');
	}

	public function addSwipeLeftAction( $obj )
	{
		$this->setAction($obj, 'swl');
	}

	public function addSwipeRightAction( $obj )
	{
		$this->setAction($obj, 'swr');
	}

	private function checkIfIcm( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}

	private function checkIfAlignment( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'r', 'c', 'l', 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					return 'r';
					break;
				case 'center' :
					return 'c';
					break;
				case 'left' :
					return 'l';
					break;
				default :
					return $val;
					break;
			}
		} else {
			return null;
		}
	}

}
