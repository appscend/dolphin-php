<?php
class toolBar
{
	public $_params;
	public function __construct()
	{
		$this->_params = array();
	}

	public function setBackgroundImage( $val , $p = null)
	{
		$prop = 'tli';
		if(isset($p)) $prop = 'tlil';
		$this->setProperty( $prop, $val );
	}

	public function setLeftCapWidth( $val , $p = null)
	{
		$prop = 'tlic';
		if(isset($p)) $prop = 'tlicc';
		$this->setProperty($prop, $val);
	}

	public function setTintColor( $val, $alpha = null )
	{
		$this->setProperty('tlt', viewHelper::color($val, $alpha));
	}

	public function setTranslucentEnabled( $val )
	{
		$this->setProperty('nbtl', $val);
	}

	public function setProperty( $param, $value, $valuePad = null  )
	{
		if(isset($param) && isset($value))
		{
			if(isset($value))
			{
				$this->_params[$param] = viewHelper::boolToLogic($value);
			}
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($value)) {
			$this->_params[$param] = $value;
			return $value;	
		}
	}

}