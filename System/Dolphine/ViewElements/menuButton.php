<?php
class menuButton extends button
{
	public function __construct( $title )
	{
		parent::__construct( $title );
	}

	public function setCancelEnabled( $val = true )
	{
		$this->setProperty('cancel', $val);
	}

	public function setDestructEnabled( $val = true )
	{
		$this->setProperty('destruct', $val);
	}
}