<?php
class mapElement extends element
{
	public function __construct($title)
	{
		parent::__construct($title);
	}

	public function setDescription( $val )
	{
		$this->setProperty('d', $val );
	}

	public function setLocation( $value, $valuePad = null )
	{
		$this->setProperty('l', $value );
	}

	public function setPinImage( $val )
	{
		$this->setProperty( 'pin', $val );
	}

	public function setPinWidth( $val )
	{
		$this->setProperty( 'pisw', $val );
	}

	public function setPinHeight( $val )
	{
		$this->setProperty( 'pish', $val );
	}

	public function setPinRadius( $val )
	{
		$this->setProperty( 'pir', $val );
	}

	public function setPinImageMode( $val )
	{
		$icm = self::checkIfIcm( $val );
		$this->setProperty( 'picm', $icm );
	}

	private function checkIfIcm( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}

}