<?php
class listHeader
{
	public $_params;

	public function __construct( $name , $namePad = null )
	{
		self::init();
		$this->setText( $name, $namePad );
	}

	private function init()
	{
		$this->_params = array();
	}

	public function setText( $val, $valPad = null )
	{
		$this->addConfig( 'tht', $val, $valPad );
	}

	public function setTextAlignment( $val, $valPad = null)
	{
		$a = self::checkIfAlignment( $val );
		$b = self::checkIfAlignment( $valPad );
		$this->addConfig( 'tha', $a, $b );
	}

	public function setImage( $val, $valPad = null )
	{
		$this->addConfig( 'thi', $val, $valPad = null );
	}

	public function setImageDisplayMode( $val, $valPad = null )
	{
		$this->setImageMode( $val, $valPad );
	}

	public function setImageMode( $val, $valPad = null)
	{
		$a = self::checkIfIcm($val);
		$b = self::checkIfIcm($valPad);
		$this->setProperty( 'thicm', $val, $valPad );
	}

	public function setHeight( $val, $valPad = null )
	{
		$this->addConfig( 'thh', $val, $valPad );
	}

	public function setVerticalAlignment( $val, $valPad = null)
	{
		$a = self::checkIfVerticalAlignment( $val );
		$b = self::checkIfVerticalAlignment( $valPad );
		$this->addConfig( 'thva', $a, $b );
	}

	public function setFont()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if( $no_args > 0 ) $this->addConfig( 'thf', $args[0], null );
		if( $no_args > 1 ) $this->setFontSize( $args[1], null );
		if( $no_args > 2 ) $this->setFontColor( $args[2], null );
	}

	public function setFontSize( $val, $valPad = null )
	{
		$this->addConfig('ths', $val, $valPad);
	}

	public function setFontColor( $val, $valPad = null )
	{
		$this->addConfig('thc', $val, $valPad);	
	}

	public function setBackgroundColor( $val, $valPad = null )
	{
		$this->addConfig('thbc', $val, $valPad);
	}

	public function addConfig( $param, $value , $valuePad = null )
	{
		if(isset($param) && isset($value))
		{
			if($param != 'es'){
				$this->_params[$param] = viewHelper::boolToLogic($value);
				if(isset($valuePad))
				{
					$param  = "pad".$param;
					$this->_params[$param] = viewHelper::boolToLogic($valuePad);
				}
			}
		}
	}

	private function checkIfIcm( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}

	private function checkIfAlignment( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'r', 'c', 'l', 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					return 'r';
					break;
				case 'center' :
					return 'c';
					break;
				case 'left' :
					return 'l';
					break;
				default :
					return $val;
					break;
			}
		} else {
			return null;
		}
	}

	private function checkIfVerticalAlignment( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 't', 'c', 'b', 'top', 'center', 'bottom' )))
		{
			switch( $val )
			{
				case 'top' :
					return 't';
					break;
				case 'center' :
					return 'c';
					break;
				case 'bottom' :
					return 'b';
					break;
				default :
					return $val;
					break;
			}
		} else {
			return null;
		}
	}

}