<?php
class buttonGroup
{
	public $_params;
	public $_buttons;
	public function __construct()
	{
		self::init();
	}

	private function init()
	{
		$this->_params = array();
		$this->_buttons = array();
	}

	public function showIfPurchaseMade( $prod )
	{
		$this->setProperty('prod', $prod );
	}

	public function showIfPurchaseNotMade( $nprod )
	{
		$this->setProperty('nprod', $nprod );
	}

	public function setPositionTolbar()
	{
		$this->setProperty( 'p', 't' );
	}

	public function setPositionNavBar( $val )
	{
		if( in_array($val, array( 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					$this->setProperty( 'p', 'rn' );
					break;
				case 'center' :
					$this->setProperty( 'p', 'tn' );
					break;
				case 'left' :
					$this->setProperty( 'p', 'ln' );
					break;
			}
		}
	}

	public function setTintColor( $val )
	{
		$this->setProperty( 'tc', $val );
	}

	public function staySelected( $val = false )
	{
		$this->setProperty( 'mom', $val );
	}

	public function addButtons()
	{
		self::addSingleLevel( func_get_args(), '_buttons' );
	}

	public function resetButtons()
	{
		self::reset('_buttons');
	}

	private function reset( $name )
	{
			$this->$name = array();
	}

	public function setProperty( $param, $value, $valuePad = null  )
	{
		if(isset($param) && isset($value))
		{
			$this->_params[$param] = viewHelper::boolToLogic($value);
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($value)) {
			$this->_params[$param] = $value;
			return $value;	
		}
	}

	private function addSingleLevel( $params , $where )
	{
		foreach ($params as $key => $value) {
            if (!is_null($value)) {
            	if(is_object($value))
            	{
            		array_push($this->$where, $value);
            	}
            }
        }
	}

	private function addMultileLevel( $params , $where )
	{
		$set_of_elements = array();
		foreach ($params as $key => $value) {
            if (!is_null($value)) {
            	if(is_object($value))
            	{
            		array_push($set_of_elements, $value);
            	}
            }
        }
        if( sizeof($set_of_elements)>0 ){
        	array_push($this->$where, $set_of_elements);
        }
	}

}