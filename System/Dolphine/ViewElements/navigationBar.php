<?php
class navigationBar
{
	public $_params;
	public function __construct( $title = null )
	{
		$this->_params = array();
		$this->setTitle($title);
	}

	public function setTitle( $val )
	{
		$this->setProperty('title', $val);
	}

	public function setBackgroundImage( $val , $p = null)
	{
		$prop = 'nbi';
		if(isset($p)) $prop = 'nbil';
		$this->setProperty( $prop, $val );
	}

	public function setLeftCapWidth( $val , $p = null)
	{
		$prop = 'nbic';
		if(isset($p)) $prop = 'nbilc';
		$this->setProperty($prop, $val);
	}

	public function setTintColor( $val , $alpha = null )
	{
		$this->setProperty('nbt', viewHelper::color($val, $alpha));
	}

	public function setTranslucentEnabled( $val )
	{
		$this->setProperty('nbtl', $val);
	}

	public function setLogoImage( $val )
	{
		$this->setProperty('nbl', $val);
	}

	public function setFont()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if($no_args > 0) $this->setProperty('nbf', $args[0]);
		if($no_args > 1) $this->setProperty('nbs', $args[1]);
		if($no_args > 2) $this->setProperty('nbc', $args[2]);
	}

	public function setTitleColor( $val )
	{
		$this->setProperty( 'nbc', $val );
	}

	public function setTitleSize( $val )
	{
		$this->setProperty( 'nbs', $val );
	}

	public function setTitleAlignment( $val )
	{
		$this->setProperty( 'nba', self::checkIfAlignment($val) );
	}

	private function checkIfAlignment( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'r', 'c', 'l', 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					return 'r';
					break;
				case 'center' :
					return 'c';
					break;
				case 'left' :
					return 'l';
					break;
				default :
					return $val;
					break;
			}
		} else {
			return null;
		}
	}

	public function setProperty( $param, $value, $valuePad = null  )
	{
		if(isset($param) && isset($value))
		{
			if(isset($value))
			{
				$this->_params[$param] = viewHelper::boolToLogic($value);
			}
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($value)) {
			$this->_params[$param] = $value;
			return $value;	
		}
	}

}