<?php
class image extends element
{
	public function __construct( $image = null )
	{
		parent::__construct( null );
		$this->setProperty('type', 'image');
		$this->setImage( $image );
	}
	
}