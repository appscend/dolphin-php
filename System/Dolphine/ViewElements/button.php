<?php
class Button
{
	public $_params;
	public function __construct( $title )
	{
		self::init();
		if(isset($title))
		{
			self::setTitle( $title );
		}
	}

	private function init()
	{
		$this->_params = array();
	}

	public function setPositionOnToolbar()
	{
		$this->setProperty( 'p', 't' );
	}

	public function setPositionOnNavBar( $val )
	{
		if( in_array($val, array( 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					$this->setProperty( 'p', 'rn' );
					break;
				case 'center' :
					$this->setProperty( 'p', 'tn' );
					break;
				case 'left' :
					$this->setProperty( 'p', 'ln' );
					break;
			}
		}
	}

	public function setAvi( $val )
	{
		$this->setProperty('avi', $val);
	}

	public function setTargetViewType( $val, $valPad = null )
	{	
		$this->setProperty( 'vt', $val, $valPad );
	}

	public function setTargetView( $val, $valPad = null )
	{
		$this->setProperty( 'px', $val, $valPad );
	}

	public function setDataForTargetView( $val, $valPad = null )
	{
		$this->setProperty( 'data', $val, $valPad );
	}
	
	public function setTitle( $val , $valPad = null)
	{
		$this->setProperty( 't', $val , $valPad );
	}

	public function setAlternativeTitle( $val, $valPad = null )
	{
		$this->setProperty( 't2', $val , $valPad );
	}

	public function setImage( $val, $valPad = null )
	{
		$this->setProperty( 'i', $val , $valPad );
	}

	public function setPadding( $val )
	{
		if(is_int($val))
		{
			$this->setProperty( 'pd', $val );
		}
	}

	public function setWidth( $val )
	{
		if(is_int($val))
		{
			$this->setProperty( 'w', $val );
		}
	}

	public function setHeight( $val )
	{
		if(is_int($val))
			{
				$this->setProperty( 'h', $val );
			}
	}

	public function setBorder( $tint_color = null )
	{
		$this->setProperty( 'br', true );
		$this->setProperty( 'tc', $tint_color );
	}

	public function showIfPurchaseMade( $prod )
	{
		$this->setProperty('prod', $prod );
	}

	public function showIfPurchaseNotMade( $nprod )
	{
		$this->setProperty('nprod', $nprod );
	}

	public function setAction( $val = null )
	{
		if( isset($val->_params) )
		{
			foreach( $val->_params as $key => $value )
			{
				$this->setProperty( $key, $value );
			}
		}
	}

	public function addAction( $val = null )
	{
		if( isset($val->_params) )
		{
			foreach( $val->_params as $key => $value )
			{
				$this->setProperty( $key, $value );
			}
		}
	}

	public function setAlternativeAction( $val = null )
	{
		if( isset($val->_params) )
		{
			foreach( $val->_params as $key => $value )
			{
				$this->setProperty( 'a'.$key, $value );
			}
		}	
	}

	public function addAlternativeAction( $val = null )
	{
		if( isset($val->_params) )
		{
			foreach( $val->_params as $key => $value )
			{
				$this->setProperty( 'a'.$key, $value );
			}
		}
	}

	public function setProperty( $param, $value, $valuePad = null  )
	{
		if(isset($param) && isset($value))
		{
			$this->_params[$param] = viewHelper::boolToLogic($value);
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($value)) {
			$this->_params[$param] = $value;
			return $value;	
		}
	}
}