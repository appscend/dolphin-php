<?php
class label extends element
{
	public function __construct( $title = null )
	{
		parent::__construct( $title );
		$this->setProperty('type', 'label');
	}

	public function setPaddingLeft( $val, $valPad = null )
	{
		$this->setProperty('lnp', $val, $valPad);
	}

	public function setPaddingRight( $val, $valPad = null )
	{
		$this->setProperty('rnp', $val, $valPad);	
	}

	public function setPaddingTop( $val, $valPad = null )
	{
		$this->setProperty('tnp', $val, $valPad);
	}

	public function setPaddingBottom( $val, $valPad = null )
	{
		$this->setProperty('bnp', $val, $valPad);
	}

}