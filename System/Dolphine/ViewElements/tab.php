<?php
class tab extends element
{
	//public $_params;
	public function __construct( $title = null )
	{
		parent::__construct( null );
		self::setTitle( $title );
	}

	public function setTitle( $val, $valPad = null )
	{
		self::setProperty( 'tt', $val, $valPad );
	}

	public function setImage( $val, $valPad = null )
	{
		self::setProperty( 'i', $val, $valPad );
	}

	public function setTargetViewType( $val, $valPad = null )
	{	
		self::setProperty( 'vt', $val, $valPad );
	}

	public function setTargetView( $val, $valPad = null )
	{
		self::setProperty( 'px', $val, $valPad );
	}

	public function setDataForTargetView( $val, $valPad = null )
	{
		self::setProperty( 'data', $val, $valPad );
	}

	public function setAvi( $val )
	{
		$this->setProperty('avi', $val);
	}

}