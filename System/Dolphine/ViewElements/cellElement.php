<?php
class cellElement
{
	public $_params;
	public $_prefix;
	public function __construct( $title = null , $prefix = null )
	{
		self::init();
		if(isset($prefix)) $this->_prefix = $prefix;
		if(isset($title)) $this->responsiveTitle($title);
	}

	private function init()
	{
		$this->_params = array();
		$this->_prefix = '';
	}

	public function setName( $value, $valuePad = null )
	{
		$this->responsiveTitle( $value, $valuePad );
	}

	public function setFont()
	{
		$args = func_get_args();
		$prefix = $this->_prefix;
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		$no_args = sizeof($args);
		if( $no_args > 0 ) $this->setProperty( 'f', $args[0], null, $prefix );
		if( $no_args > 1 ) $this->setProperty( 's', $args[1], null, $prefix );
		if( $no_args > 2 ) $this->setProperty( 'c', $args[2], null, $prefix );
		if( $no_args > 3 ) {
			if($this->_prefix == 'bdg') 
			{
				$this->setProperty( 'sc', $args[3], null, $this->_prefix );
			} else {
				$this->setProperty( 'c', $args[3], null, 's'.$this->_prefix );
			}
		}
		
	}

	public function setCellImageBackground( $val, $valPad = null )
	{
		$this->setProperty( 'ci', $val, $valPad, '' );
	}

	public function setNoLines( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		$this->setProperty( 'l', $val, $valPad, $prefix );
	}

	public function setFontSize( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		$this->setProperty( 's', $val, $valPad, $prefix );	
	}

	public function setFontColor( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		$this->setProperty( 'c', $val, $valPad, $prefix );	
	}

	public function setSelectedFontColor( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		$this->setProperty( 'c', $val, $valPad, 's'.$prefix );
	}

	public function setTextAlignment( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		$a = self::checkIfAlignment( $val );
		$b = self::checkIfAlignment( $valPad );
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		$this->setProperty( 'a', $a, $b, $prefix );
	}

	public function setBackgroundColor( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		if($this->_prefix == 'it')  $prefix = 'i';
		$this->setProperty( 'bc', $val, $valPad, $prefix );	
	}

	public function setSelectedBackgroundColor( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'dv')  $prefix = 'dvn';
		if($this->_prefix == 'it')  $prefix = 'i';
		$this->setProperty( 'bc', $val, $valPad, 's'.$prefix );
	}

	public function setImagePath( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'it')  $prefix = '';
		if($this->_prefix == 'i')  $prefix = '';
		if($this->_prefix == 'at') $prefix = 'a';
		$this->setProperty( 'i', $val, $valPad, $prefix );
	}

	public function setImageLeftCap( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'it' ) $prefix = '';
		if($this->_prefix == 'i' ) $prefix = '';
		$this->setProperty( 'ilp', $val, $valPad, $prefix );
	}

	public function setImageTopCap( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'it' ) $prefix = '';
		if($this->_prefix == 'i' ) $prefix = '';
		$this->setProperty( 'itp', $val, $valPad, $prefix );
	}

	public function setImageCornerRadius( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'it' ) $prefix = '';
		if($this->_prefix == 'i' ) $prefix = '';
		$this->setProperty( 'icr', $val, $valPad, $prefix );	
	}

	public function setImageWidth( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'it')  $prefix = 'i';
		if($this->_prefix == 'at')  $prefix = 'ai';
		if($this->_prefix == 'dv')  $prefix = 'dvi';
		$this->setProperty( 'sw', $val, $valPad, $prefix );
	}

	public function setImageHeight( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'it')  $prefix = 'i';
		if($this->_prefix == 'at')  $prefix = 'ai';
		if($this->_prefix == 'dv')  $prefix = 'dvi';
		$this->setProperty( 'sh', $val, $valPad, $prefix );
	}

	public function setImageMode( $val, $valPad = null)
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'i')  $prefix = '';
		if($this->_prefix == 'it')  $prefix = '';
		if($this->_prefix == 'at')  $prefix = 'a';
		$a = self::checkIfIcm($val);
		$b = self::checkIfIcm($valPad);
		$this->setProperty( 'icm', $val, $valPad, $prefix );
	}

	public function setAction( $obj = null )
	{
		$prefix = '';
		if($this->_prefix == 'at')  $prefix = 'a';
		if(isset($obj->_params))
		foreach( $obj->_params as $key=>$value){
			$this->setProperty( $key, $value, null, $prefix );
		}
	}

	public function addAction( $obj = null )
	{
		$prefix = '';
		if($this->_prefix == 'at')  $prefix = 'a';
		if(isset($obj->_params))
		foreach( $obj->_params as $key=>$value){
			$this->setProperty( $key, $value, null, $prefix );
		}
	}

	public function setCornerRadius( $val, $valPad = null )
	{
		$prefix = $this->_prefix;
		if($this->_prefix == 'it')  $prefix = 'i';
		if($this->_prefix == 'at')  $prefix = 'a';
		if($this->_prefix == 'dv')  $prefix = 'dvi';
		$this->setProperty( 'r', $val, $valPad, $this->_prefix );	
	}

	public function setProperty( $param, $value, $valuePad = null , $prefix = '')
	{
		if(!isset($prefix)) $prefix = $this->_prefix;
		if(isset($param) && isset($value))
		{
			$param = $prefix.$param;
			$this->_params[$param] = viewHelper::boolToLogic($value);
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_params[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	public function setWebViewContent( $value, $valuePad = null )
	{
		$this->setProperty( 'content', $val, $valPad, '' );
	}

	public function setWebViewUrl( $value, $valuePad = null )
	{
		$this->setProperty( 'url', $val, $valPad, '' );
	}

	public function setWebViewHtml( $value, $valuePad = null )
	{
		$this->setProperty( 'htmlc', $val, $valPad, '' );
	}

	public function setItm( $value, $valuePad = null )
	{
		$this->setProperty( 'itm', $val, $valPad, '' );
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param , $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($value)) {
			$this->_params[$param] = $value;
			return $value;	
		}
	}

	private function responsiveTitle( $title, $titlePad = null )
	{
		switch( $this->_prefix )
		{
			case 'i' :
				$this->_params['it'] = $title;
				$this->_prefix = 'it';
				if(isset($titlePad)) $this->_params['padit'] = $titlePad;
				break;
			case 'dv' :
				$this->_params['dvn'] = $title;
				if(isset($titlePad)) $this->_params['paddvn'] = $titlePad;
				break;
			default :
				$this->_params[$this->_prefix] = $title;
				if(isset($titlePad)) $this->_params['pad'.$this->_prefix] = $titlePad;
				break;
		}
	}

	private function checkIfIcm( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}

	private function checkIfAlignment( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'r', 'c', 'l', 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					return 'r';
					break;
				case 'center' :
					return 'c';
					break;
				case 'left' :
					return 'l';
					break;
				default :
					return $val;
					break;
			}
		} else {
			return null;
		}
	}
}