<?php
class elementTemplate
{
	public $_params;
	public function __construct()
	{
		$this->_params = array();
	}

	public function setProperty( $param, $value = null )
	{
		if(!isset($value))
		{
			if(!is_array($value) || !is_object($value))
				$this->_params[$param] = '['.$param.']';
		} else {
			if(!is_array($value) || !is_object($value))
				$this->_params[$param] = $value;
		}
	}

	public function getProperty( $param, $val = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} elseif(isset($val)) {
			$this->_params[$param] = '['.$param.']';
			return $this->_params[$param];
		}
	}
}