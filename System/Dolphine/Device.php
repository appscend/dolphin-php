<?php
interface DeviceInterface{
	public static function isPhone();
	public static function isTablet();
	public static function isIos();
	public static function isAndroid();
	public static function is_iPhone();
	public static function is_iPad();
	public static function is_androidPhone();
	public static function is_androidTablet();
	public static function timeStamp();
	public static function timeZone();
	public static function appVersion();
	public static function udid();
	public static function language();
	public static function latitude();
	public static function longitude();
}
class Device implements DeviceInterface
{
	public static function isPhone(){
		if( self::is_iPhone() || self::is_androidPhone() ) return true;
		return false;
	}

	public static function isTablet(){
		if( self::is_iPad() || self::is_androidTablet() ) return true;
		return false;
	}

	public static function isAndroid(){
		if( self::is_androidTablet() || self::is_androidPhone() ) return true;
		return false;
	}

	public static function isIos(){
		if( self::is_iPad() || self::is_iPhone() ) return true;
		return false;
	}

	//returns true if the device is an iPhone5
	public static function isiPhone5()
	{
		$device = null;
		if(isset($_POST['device'])){
			$device = $_POST['device'];
		}
		if(in_array((strtolower(substr($device, 0, 7))), array('iphone6','iphone5')) || (strtolower(substr($device, 0, 5)) == 'ipod5')){
			return true;
		}else{
			return false;
		}
	}

	public static function is_iPhone()
	{
		if(isset($_REQUEST['platform']) && ($_REQUEST['platform'] == 1))
		{
			return true;
		} else {
			return false;
		}
	}

	public static function is_iPad()
	{
		if(isset($_REQUEST['platform']) && ($_REQUEST['platform'] == 2))
		{
			return true;
		} else {
			return false;
		}
	}

	public static function is_androidPhone()
	{
		if(isset($_REQUEST['platform']) && ($_REQUEST['platform'] == 3))
		{
			return true;
		} else {
			return false;
		}
	}

	public static function is_androidTablet()
	{
		if(isset($_REQUEST['platform']) && ($_REQUEST['platform'] == 4))
		{
			return true;
		} else {
			return false;
		}
	}

	public static function timeStamp()
	{
		if(isset($_REQUEST['timestamp']))
		{
			return $_REQUEST['timestamp'];
		}
	}

	public static function timeZone()
	{
		if(isset($_REQUEST['timezone']))
		{
			return $_REQUEST['timezone'];
		}
	}

	public static function appVersion()
	{
		if(isset($_REQUEST['appversion']))
		{
			return $_REQUEST['appversion'];
		}
	}

	public static function udid()
	{
		if(isset($_REQUEST['udid']))
		{
			return $_REQUEST['udid'];
		}
	}

	public static function language()
	{
		if(isset($_REQUEST['locale']))
		{
			return $_REQUEST['locale'];
		}
	}

	public static function latitude()
	{
		if(isset($_REQUEST['lat']))
		{
			return $_REQUEST['lat'];
		}
	}

	public static function longitude()
	{
		if(isset($_REQUEST['lon']))
		{
			return $_REQUEST['lon'];
		}
	}
}
