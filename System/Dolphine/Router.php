<?php
class Router
{
    public static  $_url_elements;
    public static  $_verb;
    public static  $_parameters;
    public static  $_format;
    private static $_instance;

    public static function init()
    {
        if (self::$_instance === null)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct() {
        if(\System\Dolphine\Registry::getSetting('master_cache') == "off")
        {
            self::routeExecute();
        } else {
            if( System\Dolphine\Registry::getSetting('cache') === true )
            {
                $data = DCache::retrieveObject();
                if($data)
                {
                    ob_end_flush();
                    imGen::generateView( $data['source'], $data['gzip'], false, true, null );
                }  else {
                    self::routeExecute();
                }
            } else {
                self::routeExecute();
            }
        }

    }

    private static function routeExecute()
    {
        self::$_verb = $_SERVER['REQUEST_METHOD'];
        if(isset($_SERVER['PATH_INFO'])){
            self::$_url_elements = explode( DIRECTORY_SEPARATOR, $_SERVER['PATH_INFO'] );

            if ( strlen(self::$_url_elements[0]) != 0 ){
                $classname = ucfirst( self::$_url_elements[0] );
            }else{
                $classname = ucfirst( self::$_url_elements[1] );
            }

            self::loadFile($classname, self::$_url_elements);

        } else {

            $default_controller = \System\Dolphine\Registry::getSetting('default_controller');

            if(strlen($default_controller) > 0 ){
                self::loadFile($default_controller);
            } else {
                DolphinError::writeError("Verify default controller in App/application.php");
            }

        }
        ob_end_flush();
    }

    public static function loadFile( $classname, $_url_elements = null ){
        self::parseIncomingParams();
        $load_file = null;
        if (is_readable(APPPATH . 'Controllers'. DIRECTORY_SEPARATOR . $classname . '.php')) {
            $load_file = APPPATH . 'Controllers'. DIRECTORY_SEPARATOR . $classname . '.php';
        }
        if( isset($load_file) ){
                $_ret = new ControllerDefinition();
                require $load_file;
                $_ret->setName( $classname );
                if(isset($_url_elements[2]) && strlen($_url_elements[2])>0){
                    $_ret->setAction( lcfirst( $_url_elements[2]) );
                } else {
                    $_ret->setAction('index');
                }
                try {
                    $nameClass = $_ret->getName();
                    $objRet = new $nameClass;

                    $method = $_ret->getAction();
                    $new_method = self::deviceVersion( $method );

                    if( method_exists( $objRet, $new_method ) ){
                        System\Dolphine\Registry::storeDeposit(self::$_parameters);
                        return call_user_func( array( $objRet, $new_method ) );
                    } elseif( method_exists( $objRet, $method ) ) {
                        System\Dolphine\Registry::storeDeposit(self::$_parameters);
                        return call_user_func( array( $objRet, $method ) );
                    } else {
                        DolphinError::writeError('Method '. $_ret->getAction() . ' is missing from '. $nameClass .'.php');
                    }
                } catch (\Exception $e) {
                    DolphinError::writeError( $e->getMessage() );
                }
        } else {
            DolphinError::writeError('Controller named '.$classname.' does not exist');
            return true;
        }
    }


    public static function parseIncomingParams() {
        $parameters = array();
        foreach($_REQUEST as $key=>$value)
        {
            $parameters[$key] = $value;
        }
        if(is_array($parameters) || is_object($parameters)){
            foreach ($parameters as $key => $value) {
                $parameters[$key] = self::unserializeData($value);
            }
        } else {
            $parameters = unserializeData($parameters);
        }
        self::$_parameters = $parameters;
    }

    public static function unserializeData($data){
        if((json_decode($data) != NULL) ? true : false){
                return json_decode($data, true);
        } else {
                return $data;
        }
    }

    private static function deviceVersion( $method )
    {
        $_method = $method;
        if(Device::is_androidPhone()){
            $_method = $method.'_android';
        }
        if(Device::is_androidTablet()){
            $_method = $method.'_androidTablet';
        }
        if(Device::is_iPad()){
            $_method = $method.'_iPad';
        }
        return $_method;
    }

    private static function gabiNoCache()
    {
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store");
        header("Cache-Control: no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

}
