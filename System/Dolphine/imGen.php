<?php
namespace System\Dolphine;
/**
 * IGNITE MARKUP GENERATOR class
 *
 * @author Lacatusu Mihai Daniel
 * @version 0.1
 *
 **/

class imGen {

  public static function generateView( array $source, $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null )
  {
    if(\System\Dolphine\Registry::getSetting('master_cache') == "off"){
      self::gabiNoCache();
      $iml = self::arrayToXMLString( $source );
      if($gzip === true){
          $iml = self::gzipData( $iml );
      }
      echo $iml;
    } else {
      self::controlCache( $source, $gzip, $cache, $device_cache, $cache_time_out );
    }
  }

  private static function controlCache( array $source, $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null )
  {
    if( $cache === false ) {
      if( ! $device_cache ){
        self::gabiNoCache();
      }
    }
    if( ! isset($cache) ){
      $cache = \System\Dolphine\Registry::getSetting('cache');
    }
    if( ! isset($gzip) ){
      $gzip = \System\Dolphine\Registry::getSetting('gzip');
    }
    if($cache === true) {
      $_time_cache = 900;
      $conf_time_cache = \System\Dolphine\Registry::getSetting('cache_time_out');
      if(isset($conf_time_cache)){
        $_time_cache = $conf_time_cache;
      }
      if(isset($cache_time_out)){
        $_time_cache = $cache_time_out;
      }
      \DCache::storeObject($source, $gzip, $_time_cache);
    }
    $iml = self::arrayToXMLString( $source );
    if($gzip === true){
      $iml = self::gzipData( $iml );
    }
  echo $iml;
  }

  private static function gabiNoCache()
  {
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store");
    header("Cache-Control: no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
  }

  private static function gzipData( $data )
  {
    $gzdata = gzencode($data, 9);
    $date = date_create();
    header('Content-Type: application/x-download');
    header('Content-Encoding: gzip');
    header('Content-Length: '.strlen($gzdata));
    header('Content-Disposition: attachment; filename="output'.date_timestamp_get($date).'.gzip"');
    return $gzdata;
  }

  public static function arrayToDOMDocument(array $source, $rootTagName = 'par')
  {
    $version = '1.0';
    $encoding = 'UTF-8';
    $document = new \DOMDocument($version, $encoding);
    $document->appendChild(self::createDOMElement($source, $rootTagName, $document));

    return $document;
  }

  /**
   * @param array $source
   * @param string $rootTagName
   * @param bool $formatOutput
   * @return string
   */
  public static function arrayToXMLString(array $source, $rootTagName = 'par', $formatOutput = true)
  {
    $document = self::arrayToDOMDocument($source, $rootTagName);
    $document->formatOutput = $formatOutput;

    return $document->saveXML();
  }

  /**
   * @param mixed $source
   * @param string $tagName
   * @param DOMDocument $document
   * @return DOMNode
   */
  private static function createDOMElement($source, $tagName = null, \DOMDocument $document, $element = null)
  {
    if ( !is_array($source) )
    {
      if( strlen($source) < 1 ) $source ="";
      $element = $document->createElement($tagName);
      $element->appendChild($document->createCDATASection($source));
      return $element;
    }

      $element = $document->createElement($tagName);

      foreach ($source as $key => $value)
      {
        self::createDOMChild($value, $key, $document, $element);
      }

    return $element;
  }

  private static function createDOMChild($value, $key, $document, $element = null)
  {
      if(is_string($key))
        {
            if(is_array($value) || is_object($value))
            {
              if(sizeof($value) > 0)
              $element->appendChild(self::createDOMElement($value, $key, $document));
            } else {
              if( strlen($value) < 1 ) $value ="";
              $element->appendChild(self::createDOMElement($value, $key, $document));
            }
        } else {
            if(is_array($value) || is_object($value))
            {
              if(sizeof($value) > 0){
                foreach ($value as $skey => $el_value)
                self::createDOMChild( $el_value, $skey, $document, $element);
              }
           }
        }
  }

}