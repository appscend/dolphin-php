<?php
namespace System\Dolphine;
use \Memory as Memory;

class Registry
{
	protected static $_storage = null;
	
	public static function setSetting( $pName, $pValue )
	{
		self::_getStorage()->setVariable( 0, $pName, $pValue );
		
	} // end setSetting
	
	public static function getSetting( $pName, $pDefaultValue = null )
	{
		return self::_getStorage()->getVariable( 0, $pName, $pDefaultValue );
		
	} // end getSetting

	public static function setData( $pName, $pValue )
	{
		self::_getStorage()->setVariable( 1, $pName, $pValue );

	} // end setData

	public static function getData( $pName, $pDefaultValue = null )
	{
		return self::_getStorage()->getVariable( 1, $pName, $pDefaultValue );

	} // end getData

	public static function getSettings()
	{
		return self::_getStorage()->getSettings();

	} // end getSettings

	public static function storeSettings( array $data )
	{
		self::_getStorage()->storeSettings($data);

	} // end storeSettings

	public static function storeDeposit( array $data )
	{
		self::_getStorage()->storeDeposit($data);

	}

	public static function getDeposit()
	{
		return self::_getStorage()->getDeposit();

	} // end getStorage
	
	public static function getAll()
	{
		return self::_getStorage()->getAll();
		
	} // end getAll
	
	/**
	 * 
	 * @return \StorageAbstract
	 */
	protected static function _getStorage()
	{
		if ( is_null( self::$_storage ) )
		{
			self::$_storage = new Memory();
		}
		return self::$_storage;
		
	} // end _getStorage
	
}