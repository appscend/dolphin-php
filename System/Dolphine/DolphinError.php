<?php
class DolphinError
{
    private static $_instance;

    public static function init()
    { 
        if (self::$_instance === null)
        { 
            self::$_instance = new self();
        } 
        return self::$_instance;
    }
    
    public static function writeError($message){
        header("HTTP/1.0 404 Not Found");
        echo '<?xml version = "1.0" encoding = "UTF-8"?><par><las><la><a>alertdebug:</a><pr><![CDATA['.$message.']]></pr></la></las></par>';
        die();
    } 

    public static function customError($level,$message,$file,$line,$context){
        $msg = '';
        $msg .= "$message";
        $msg .= "Error on line :". $line;
        $msg .= "$file";
        echo '<?xml version = "1.0" encoding = "UTF-8"?><par><las><la><a>alertdebug:</a><pr><![CDATA['.$msg.']]></pr></la></las></par>';
        die();
    }

    public static function customexceptionError( $e )
    {   
        echo '<?xml version = "1.0" encoding = "UTF-8"?><par><las><la><a>alertdebug:</a><pr><![CDATA['.$e->getMessage().']]></pr></la></las></par>';
        die();
    }

    public static function handleShutdown()
    {
        $error = error_get_last();
        if( $error !== NULL )
        {
            $info = "[SHUTDOWN] file:".$error['file']." | line:".$error['line']." | msg:".$error['message'].PHP_EOL;
            self::writeError( $info );
        } 
        else {
            return false;
            // self::writeError("SHUTDOWN");
        }
    }

}