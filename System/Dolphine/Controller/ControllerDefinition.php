<?php
namespace System\Dolphine\Controller;
class ControllerDefinition
{
	protected $_name;
	protected $_action;
	protected $_params;

	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @param field_type $_name
	 */
	public function setName($_name) {
		$this->_name = $_name;
		return $this;
	}

	/**
	 * @return the $_action
	 */
	public function getAction() {
		return $this->_action;
	}

	/**
	 * @param field_type $_action
	 */
	public function setAction($_action) {
		$this->_action = $_action;
		return $this;
	}

	/**
	 * @param field_type $_params
	 */
	public function setParams($params)
	{
		$this->_params = $params;
	}

	/**
	 * @return the $_params
	 */
	public function getParams()
	{
		if (isset($this->_params)) return $this->_params;
		else return null;
	}

}