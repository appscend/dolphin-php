<?php
namespace System\Dolphine\Controller;
class ControllerAbstract
{
	public function __construct(){}

	public static function base_url( $url = null )
	{
		if(!isset($url)) $url = '';
		return \System\Dolphine\Registry::getSetting('base_url').$url;
	}

	public static function public_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('public_url').$image;
	}

	public static function local_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('local_url').$image;
	}

	public static function cms_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('cmsbaseURL').$image;
	}

	public static function relative_path( $file = null )
	{
		return \System\Dolphine\Registry::getSetting('d_relative_path').$file;
	}

	public static function params( $name = null , $value = null )
	{
		$params = \System\Dolphine\Registry::getDeposit();
		if(isset( $name ))
		{
			if(isset( $params[$name] )){
				return $params[$name];
			} else {
				if(isset($value))
				{
					$params[$name] = $value;
					\System\Dolphine\Registry::setData( $name, $value );
					return $value;
				} else {
					if( \System\Dolphine\Registry::getData( 'debugError' ) == true){
						return 'Parameter '.$name.' does not exist';
					} else {
						return false;
					}
				}
			}
		}
		return $params;
	}

	public static function data( $name = null , $value = null )
	{
		return self::params( $name, $value );
	}

	public static function loadModel($name)
	{
		require(APPPATH .'Models/'. ucfirst($name) .'.php');
	}

	public static function loadHelper($name)
	{
		require(APPPATH .'Helpers/'. ucfirst($name) .'.php');
	}

	public function loadView( $name, $data = null )
	{

		require(APPPATH .'Views'. DIRECTORY_SEPARATOR . $name .'.php');
		$class_name = explode( DIRECTORY_SEPARATOR, $name );
		$class_name = end($class_name);

		$view = new $class_name();
		if(is_array($data) || is_object($data))
		{
			foreach ($data as $key => $value) {
				$view->setParam( $key, $value );
			}
		}
		$view->index();
	}
	
	public static function redirect($loc)
	{
		header('Location: '. self::base_url() . $loc);
	}

	public function getMethods( $class, $parent = null )
	{
		if(isset($parent)) {
			return array_diff(get_class_methods($class), get_class_methods($parent));
		} else {
			return get_class_methods($class);
		}
	}

}