<?php
class jsFunction
{
	public $_params;
	public function __construct( $param )
	{
		$this->_params = array();
		$this->setProperty('name', $param);
	}

	public function params()
	{
		self::addParams( func_get_args() );
	}

	public function args()
	{
		self::addParams( func_get_args() );	
	}

	public function setName( $val )
	{
		$this->setProperty('name', $param);
	}

	public function body( $content = null )
	{
		$this->content($content);
	}

	public function content( $content = null )
	{
		$this->setProperty('content', $content);
	}

	public function setProperty( $param, $value )
	{
		if(isset($param) && isset($value) )
		{
			if(!is_array($param) || !is_object($param) || !is_array($value) || !is_object($value))
			{
				$this->_params[$param] = $value;
			}
		}
	}

	public function getProperty( $param, $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} else {
			if(isset($value))
			{
				$this->_params[$param] = $value;
				return $value;
			}
		}
	}

	private function addParams( $params )
	{
		$full_params = '';
		$no_params = sizeof($params);
		$k = 0;
		foreach ($params as $key => $value) 
		{
			$k += 1;
            if (!is_null($value)) 
            {
            	if( $k != $no_params ) {
        			$full_params .= $value."::";
        		} else {
        			$full_params .= $value;
        		}
        	}
		}
		$this->_params['args'] = $full_params;
	}
}