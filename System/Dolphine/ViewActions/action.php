<?php
defined("__ACTION_ALERT__") or define("__ACTION_ALERT__", "alert:");
defined("__ACTION_PUSH__") or define("__ACTION_PUSH__", "p:");
defined("__ACTION_BACK__") or define("__ACTION_BACK__", "pop:");
class action 
{
	public $_params;

	public function __construct( $type_action = null )
	{
		$this->_params = array();
		if(isset($type_action)){
			if( is_array($type_action) || is_object($type_action) )
			{
				self::objecToAction($type_action);
			} else {
				$this->type($type_action);
			}
		}
	}

	public function restrictAccesWithPurchase( $id , $bool = null , $alt = null )
	{
		$prod = 'aprod';
		if($bool)  
			{
				$this->setProperty('dprod', 'yes');
			} else {
				$this->setProperty('dprod', 'no');
			}
		$this->setProperty('aprod', $id);
		/// to do !!!!!!!!!!!!!!!!!!
	}

	public function type( $type_action )
	{
		$this->_params['a'] = $type_action;
	}

	public function params( $params = null )
	{
		self::addParams( func_get_args() );
	}

	public function delay( $val = 0 )
	{
		$this->setProperty('del', $val );
	}

	public function targetView( $val = null )
	{
		$this->performOn($val);
	}

	public function performOn( $val = null )
	{
		$this->setProperty('tavi', $val );
	}

	public function confirm( $val = null )
	{
		$this->setProperty('conf', $val );
	}

	public function setProperty( $param, $value )
	{
		if(isset($param) && isset($value) )
		{
			if(!is_array($param) || !is_object($param) || !is_array($value) || !is_object($value))
			{
				$this->_params[$param] = $value;
			}
		}
	}

	public function unsetProperty( $param )
	{
		if(isset($this->_params[$param]))
		{
			unset($this->_params[$param]);
		}
	}

	public function getProperty( $param, $value = null )
	{
		if(isset($this->_params[$param]))
		{
			return $this->_params[$param];
		} else {
			if(isset($value))
			{
				$this->_params[$param] = $value;
				return $value;
			}
		}
	}

	public function setCondition()
	{
		$k = 0;
		$rsk = null;
		$rsv = null;
		$params = func_get_args();
		$no_params = sizeof(func_get_args());
		foreach ($params as $key => $value) 
		{
			$k += 1;
			if($k == 1)
			{
				if(!is_array($value) || !is_object($value)) $rsk = $value;
			}
			if(($k == 2) && is_object($value))
			{
				if(isset($value->_params))
				{
					$new_params = array();
					foreach ($value->_params as $key => $valueb) {
						$new_params['rs'.$key] = $valueb;
					}
					$this->_params = array_merge($this->_params, $new_params);
				}
			}
			if($k == 3)
			{
				if(!is_array($value) || !is_object($value)) $rsv = $value;
			}
			if($k > 3)
			{
				break;
			}
		}
		if(isset($rsk)) $this->_params['rsk'] = $rsk;
		if(isset($rsv)) $this->_params['rsv'] = $rsv;
	}

	private function objecToAction( $obj )
	{
		foreach ($obj as $key => $value) {
			if(! is_array($value) && ! is_object($value) )
			$this->_params[$key] = $value;
		}
	}

	private function addParams( $params )
	{
		$full_params = '';
		$no_params = sizeof($params);
		$k = 0;
		foreach ($params as $key => $value) 
		{
			$k += 1;
        	if( $k != $no_params ) {
        		if(is_array( $value) || is_object( $value ))
        		{
					$full_params .= json_encode($value)."::";
        		} else {
    				$full_params .= $value."::";
    			}
    		} else {
    			if(is_array( $value) || is_object( $value ))
        		{
					$full_params .= json_encode($value);
        		} else {
    				$full_params .= $value;
    			}
    		}
		}
		$this->_params['pr'] = $full_params;
	}
}