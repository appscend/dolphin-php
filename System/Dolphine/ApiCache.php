<?php
class ApiCache
{
  private static $_instance;
  private static $_memcache;
  private static $_key;

  public static function init()
    {
        if (self::$_instance === null)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // protected -> implement this;
    public function __construct()
    {
        try {
            self::$_memcache = new \Memcache;
            self::$_memcache->connect('localhost', 11212);
        } catch (Exception $e) {
            \System\Dolphine\Registry::setSetting('master_api_cache', 'off');
        }
    }

    public static function getCachedData()
    {
      return self::$_memcache->get(self::$_key);
    }

    public static function setCachedData( $data, $_time_cache = null )
    {
      if( !isset($_time_cache) ) $_time_cache = 900;
      self::$_memcache->set(self::$_key, $data, false, $_time_cache);
    }

    public static function removeCachedData()
    {
      if(self::$_memcache->get(self::$_key)) {
          self::$_memcache->delete(self::$_key);
        }
    }

    public static function setKey( $data )
    {
      $key = hash('md5', $data);
      self::$_key = $key;
    }

    public static function getKey()
    {
      return self::$_key;
    }

    public static function flushCache()
    {
      self::$_memcache->flush();
    }

}
