<?php
class Memory extends StorageAbstract
{
	protected $_stDeposit;
	protected $_stSettings;
	
	public function __construct()
	{
		$this->emptyStorage();
		
	} // end __construct()
	
	
	public function setVariable( $in, $pName, $pValue )
	{
		if( $in === 0 ) {
			$this->_stSettings[$pName] = $pValue;
		}

		if( $in === 1 ) {
			$this->_stDeposit[$pName] = $pValue;
		}

		//return $this;
		
	} // end setVariable
	
	public function getVariable( $in, $pName, $pDefaultValue = null )
	{
		$_retValue = $pDefaultValue;
		if( $in === 0) {
			if( isset( $this->_stSettings[$pName] ) )
			{
				$_retValue = $this->_stSettings[$pName];
			}
		}

		if( $in === 1) {
			if( isset( $this->_stDeposit[$pName] ) )
			{
				$_retValue = $this->_stDeposit[$pName];
			}
		}
		return $_retValue;
		
	} // end getVariable
	
	public function emptyStorage()
	{
		$this->_stDeposit = array();
		$this->_stSettings = array();

	} // end emptyStorage

	public function storeSettings( $data )
	{
		$this->_stSettings = $data;

	} // end storeSettings

	public function getSettings()
	{
		return $this->_stSettings;

	} // end getSettings


	public function storeDeposit( $data )
	{
		$this->_stDeposit = $data;

	} // end storeDeposit

	public function getDeposit()
	{
		return $this->_stDeposit;

	} // end getDeposit
	
	public function getAll()
	{
		$all = array( 
			'settings' => $this->_stSettings,
			'data' => $this->_stDeposit
		);

		return $all;
	} // end getAll
	
}