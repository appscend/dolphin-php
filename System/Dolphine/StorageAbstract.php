<?php
abstract class StorageAbstract{
	public abstract function setVariable( $in, $pName, $pValue );
	public abstract function getVariable( $in, $pName, $pDefaultValue = null );
	public abstract function getSettings();
	public abstract function getDeposit();
	public abstract function emptyStorage();
	public abstract function getAll();
}