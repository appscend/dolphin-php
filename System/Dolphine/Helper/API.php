<?php
namespace System\Dolphine\Helper;
use \Device as Device;
class API
{
    private static $_appId = null;
    private static $_key = null;
    private static $_instance;
    private static $_isGzipped;
    private static $_endPointURL = 'https://interface.appcend.com/apiv1.php';

    /**
     * [init description]
     * @return [type] [description]
     */
    public static function init()
    { 
        if (self::$_instance === null)
        { 
            self::$_instance = new self();
        } 
        return self::$_instance;
    }
    /**
     * [setConf description]
     * @param [type] $appId [description]
     * @param [type] $key   [description]
     */
    public static function setConf( $appId = null, $key = null , $gzip = false )
    {
        self::$_appId = $appId;
        self::$_key = $key;
        if(!isset($gzip)) $gzip = false;
        self::$_isGzipped = $gzip;
    }
    /**
     * create API request
     * @param  string $action the api function name
     * @param  object $data   structured data
     * @return json
     */
    public static function createRequest( $action , $data = null )
    {
        if(isset($data))
        {
            $data = self::objectToArray($data);
        }
        return self::response( $action, $data );
    }

          ////////////////////////////////
         //                            //
        // CMS ITEMS RELATED HELPERS  //
       //                            //
      //////////////////////////////// 

    /**
     * This function creates a new item in the specified category of the CMS.
     * @param  integer $catId       the ID of the category to put the item in
     * @param  string $title        title of the item
     * @param  array $data          array of key - values ('date' => '', 'sortDescription' => '', 'webviewLink' => '', 'description' => '', 'geolocation' => '' , 'videolink' => ' , )
     * @param  array $customFields  array of key - value pairs. the key represents the field name, the value represents the field value
     * @return json
     */
    public static function putItem( $catId, $title, $data = null, $customFields = null )
    {
        $action = 'putItem';
        if(isset($data))
        {
            $data = self::objectToArray($data);
        } else {
            $data = array();
        }
        if(is_array($customFields) || is_object($customFields))
        {
            $data = array_merge($data, array('customFields' => json_encode($customFields)));
        }
        $data = array_merge($data, array('catId' => $catId, 'title' => $title));
        return self::response( $action, $data );
    }

    /**
     * This function allows you to edit an item. The only fields that will be replaced will be the ones used in $data and/or $customFields, all fields are optional.
     * @param  integer $itemId       the ID of the item to edit
     * @param  array $data         array of key - values ('date' => '', 'sortDescription' => '', 'webviewLink' => '', 'description' => '', 'geolocation' => '' , 'videolink' => ', 'price' => '', )
     * @param  array $customFields array of key - value pairs
     * @return json
     */
    public static function editItem( $itemId , $catId,  $data = null, $customFields = null ){
        $action = 'editItem';
        if(isset($data))
        {
            $data = self::objectToArray($data);
        } else {
            $data = array();
        }
        if(is_array($customFields) || is_object($customFields))
        {
            $data = array_merge($data, array('customFields' => json_encode($customFields)));
        }
        $data = array_merge($data, array('itemId' => $itemId, 'catId' => $catId));
        return self::response( $action, $data );
    }

    /**
     * This function creates a relation between two items through a specified relation type. An example for such an action is relating a recipe with a related video through the "Related Videos" relation.
     * @param  integer $itemIdA     the itemId of the first item
     * @param  integer $itemIdB     the itemId of the second item
     * @param  integer $rtId        the ID of the relation type
     * @param  string $qualifierA string for the first qualifier of the relation
     * @param  string $qualifierB string for the second qualifier of the relation
     * @return json
     */
    public static function relateItem( $itemIdA, $itemIdB, $rtId, $qualifierA, $qualifierB )
    {
        $action = 'relateItem';
        $data = array(
            'item1' => $itemIdA,
            'item2' => $itemIdB,
            'rtId'   => $rtId,
            'qualifier' => $qualifierA,
            'qualifier2' => $qualifierB,
            );
        return self::response( $action, $data );
    }

    /**
     * This API function deletes an item from the CMS.
     * @param  integer $itemId (the ID of the item to be deleted
     * @return json
     */
    public static function deleteItem( $itemId )
    {
        $action = 'deleteItem';
        $data = array( 'itemId' => $itemId );
        return self::response( $action, $data );
    }

    /**
     * This API function retrieves information stored in a CMS item.
     * @param  integer $itemId       the ID of the item to be edited
     * @param  any $extended         if extended is set the function will return related items, related users, tags and the image gallery
     * @param  any $customFields     if customFields is set the function will return custom fields for the item
     * @return json
     */
    public static function getItem( $itemId, $extended = null, $customFields = null )
    {
        $action = 'getItem';
        $data = array('itemId' => $itemId);
        if(isset($extended))
        {
            $data = array_merge($data, array('extended' => 1));
        }
        if(isset($customFields))
        {
            $data = array_merge($data, array('customFields' => 1));
        }
        return self::response( $action, $data );
    }

    /**
     * This API function retrieves information stored in a CMS category, and optionally, nested categories of a parent category.
     * @param  integer $catId       
     * @param  array $data          array of key - values ('random' => 'yes/no', 'limit' => '', 'orderBy' => '', 'orderDir' => '', 'radius' => '' , 'getCommentCount' => ' , )
     * @param  any $extended     if extended is set the function will return related items, related users, tags and the image gallery
     * @param  any $customFields if customFields is set the function will return custom fields for the item
     * @return json
     */
    public static function getItems( $catId, $data = null, $extended = null, $customFields = null )
    {
        $action = 'getItems';
        if(isset($data))
        {
            $data = self::objectToArray($data);
        } else {
            $data = array();
        }
        if(isset($extended))
        {
            $data = array_merge($data, array('extended' => 1));
        }
        if(isset($customFields))
        {
            $data = array_merge($data, array('customFields' => 1));
        }
        $data = array_merge($data , array('catId' => $catId) );
        return self::response( $action, $data );
    }

    /**
     *  This function adds a new custom item field for the items in the app content.
     *  The field type is an integer with values 1/2/3 or textbox/dropdown/hidden
     *  custom field (accessible only from the CMS).
     * @param  string $fieldName the name of the custom field
     * @param  string $fieldType the type of the custom field
     * @return json
     */
    public static function defineItemfield( $fieldName , $fieldType )
    {
        $action = 'defineItemfield';
        $continue = true;
        switch ($fieldType) {
            case 'textbox':
                $fieldType = 1;
                break;
            case 'dropdown':
                $fieldType = 2;
                break;
            case 'hidden':
                $fieldType = 3;
                break;
            default:
                if(in_array($fieldType, array(1, 2, 3)))
                {
                    $continue = false;
                }
                break;
        }
        if($continue)
        {
            $data = array( 'fieldName' => $fieldName, 'fieldType' => $fieldType );
            return self::response( $action, $data );
        }
    }

    /**
     * This function sets the value of the custom field of an item.
     * @param integer $itemId     the ID of the item to be edited
     * @param string $fieldName   the name of the custom field
     * @param string $fieldValue  value of the field
     * @return json
     */
    public static function setItemfield( $itemId, $fieldName, $fieldValue )
    {
        $action = 'setItemfield';
        $data = array( 'itemId' => $itemId, 'fieldName' => $fieldName, 'fieldType' => $fieldValue );
        return self::response( $action, $data );
    }

    /**
     * This function deletes a custom field from the database.
     * @param  integer $fieldName  the name of the custom field to be deleted
     * @return json
     */
    public static function undefineItemfield( $fieldName )
    {
        $action = 'undefineItemfield';
        $data = array( 'fieldName' => $fieldName );
        return self::response( $action, $data );
    }

    /**
     * This function retrieves the values of the custom fields of a CMS item
     * @param  [type] $itemId the ID of the item from where you get the custom fields
     * @return json
     */
    public static function getItemfields( $itemId )
    {
        $action = 'getItemfields';
        $data = array( 'itemId' => $itemId );
        return self::response( $action , $data );
    }

    /**
     * This function will delete the value of a custom field for a CMS item.
     * @param  integer $itemId    the ID of the item from where you will delete the value of the custom field
     * @param  string $fieldName name of the customField
     * @return json
     */
    public static function unsetItemfield( $itemId, $fieldName )
    {
        $action = 'unsetItemfield';
        $data = array( 'itemId' => $itemId , 'fieldName' => $fieldName);
        return self::response( $action , $data );  
    }

    /**
     * This function lists the defined custom field for a  CMS item
     * @param  integer $itemId the id of the item
     * @return json
     */
    public static function listItemfields( $itemId )
    {
        $action = 'listItemfields';
        $data = array( 'itemId' => $itemId );
        return self::response( $action , $data );
    }

    /**
     * This function will return all the items in the app that are linked to a specific tag.
     * @param  string $tag      the name of the tag
     * @param  any $extended    if extended is set the function will return related items, related users, tags and the image gallery
     * @return json
     */
    public static function getItemsForTag( $tag, $extended = null )
    {
        $action = 'getItemsForTag';
        $data = array('tag' => $tag);
        if(isset($extended))
        {
            $data = array_merge($data, array('extended' => 1));
        }
        return self::response( $action , $data );
    }

    /**
     * This function returns all tags in an app.
     * @return json
     */
    public static function getAllTags()
    {
        $action = 'getItemsForTag';
        return self::response( $action );
    }

    /**
     * You can add comments to a CMS Item using this function.
     * @param integer $itemId id of the item from cms
     * @param strinf $text    the comment text
     * @return json
     */
    public static function addComment( $itemId, $text, $custom_udid = null )
    {
        $action = 'addComment';
        $udid = '';
        if( isset($custom_udid) )
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        $data = array(
            'itemId' => $itemId,
            'text' => $text,
            'udid' => $udid,
            );
        return self::response( $action, $data );
    }

    /**
     * This function returns the comments associated with an item.
     * @param  integer $itemId   the id of the item from cms
     * @param  any $extended     if extended is set the function will return related items, related users, tags and the image gallery
     * @return json
     */
    public static function getComments( $itemId, $extended = null )
    {
        $action = 'getComments';
        $data = array( 'itemId' => $itemId );
        if(isset($extended))
        {
            $data = array_merge($data, array('extended' => 1));
        }
        return self::response( $action , $data );
    }

    /**
     * This function can be used for rating an item.
     * @param integer $itemId [description]
     * @param integer $rating rating value
     * @return json
     */
    public static function addRating( $itemId, $rating, $custom_udid = null )
    {
        $action = 'addRating';
        $udid = '';
        if( isset($custom_udid) )
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        $data = array( 'itemId' => $itemId, 'udid' => $udid );
        if(is_int($rating))
        {
            $data = array_merge($data, array( 'rating' => $rating));
        }
        return self::response( $action , $data );
    }

    /**
     * This function will return the average rating of an item and the number of ratings submitted
     * @param  integer $itemId   item id from cms
     * @param  any $extended if set the function will also return data about the user who posted each rating.
     * @return json
     */
    public static function getRatings( $itemId, $extended = null )
    {
        $action = 'getRatings';
        $data = array( 'itemId' => $itemId );
        if(isset($extended))
        {
            $data = array_merge($data, array('extended' => 1));
        }
        return self::response( $action , $data );
    }

    /**
     * This function will return the items in an app, ordered by rating, and, optionally, filtered by a minimum creation date.
     * @param  date $minDate  a date with a format compatible with strtotime
     * @return json
     */
    public static function getAllItemsByRating( $minDate = null )
    {
        $action = 'getAllItemsByRating';
        if(isset($minDate))
        {
            return self::response( $action , array($minDate) );
        } else {
            return self::response( $action );
        }
    }

          ////////////////////////////////
         //                            //
        // CMS CATEGS RELATED HELPERS //
       //                            //
      //////////////////////////////// 

    /**
     * This function returns all the categories in an app. 
     * @return json
     */
    public static function getCategs()
    {
        $action = 'getCategs';
        return self::response( $action );
    }

    /**
     * This function adds a category to the database. 
     * @param  string $name     name for the Category
     * @param  integer $parentId the ID of the parent category, optional
     * @return json
     */
    public static function putCategory( $name , $parentId = null )
    {
        $action = 'putCategory';
        $name_encoded = base64_encode($name);
        $data = array( 'name' => $name_encoded, 'base64encoded' => 1 );
        if(isset($parentId)) $data = array_merge($data, array('parentId' => $parentId));
        return self::response( $action, $data );
    }

    /**
     * This function deletes a category (and it’s items) from the database 
     * @param  integer $catId the id of the Category
     * @return json
     */
    public static function deleteCategory( $catId )
    {
        $action = 'deleteCategory';
        $data = array('catId' => $catId );
        return self::response( $action, $data );
    }

    /**
     * This function sets the fields of the item associated with a category (the category metadata). 
     * @param  array $data         array('data' => 'any string compatible with strtotime', 'shortDescription' => '', 'webviewLink' => '', etc)
     * @param  array $customFields Optional. Array with your key -> value set.
     * @return json
     */
    public static function putMetadata( $data, $customFields = null )
    {
        $action = 'putMetadata';
        if(is_array($customFields) || is_object($customFields))
        {
            $data = array_merge($data, array('customFields' => json_encode($customFields)));
        }
        return self::response( $action, $data );
    }

    /**
     * This function returns the subcategories of a category
     * @param  integer $parentId     [description]
     * @param  string $orderBy      order subcategories by (title, rating,)
     * @param  any $metaData     if present, the function will also return the metadata of each category, it’s related items, ratings, comments and tags.
     * @param  any $customFields if set the function will also return the custom fields.
     * @param  any $random       the result will be randomized
     * @return json
     */
    public static function getCategoriesForParent( 
        $parentId , $orderBy = null, $metaData = null, $customFields = null, $random = null
    )
    {
        $action = 'getCategoriesForParent';
        $data = array('parentId' => $parentId );
        if(isset($orderBy)) $data = array_merge($data, array('orderBy' => $orderBy));
        if(isset($metaData)) $data = array_merge($data, array('metaData' => 1));
        if(isset($customFields)) $data = array_merge($data, array('customFields' => 1));
        if(isset($random)) $data = array_merge($data, array('random' => 1));
        return self::response( $action, $data );
    }

    /**
     * This function returns the categories linked to a specific tag.
     * @param  string $tag       the name of the tag
     * @param  any $metaData  if present, the function will return the category metadata, related items, tags, custom fields, galleries and file galleries
     * @param  any $justCount if present, the function will only return the number of related items for each category metadata
     * @return json
     */
    public static function getCategoriesForTag( $tag, $metaData = null, $justCount = null )
    {
        $action = 'getCategoriesForTag';
        $data = array( 'tag' => $tag );
        if(isset($metaData)) $data = array_merge($data, array('metaData' => 1));
        if(isset($justCount)) $data = array_merge($data, array('justCount' => 1));
        return self::response( $action, $data );
    }

    /**
     * This function returns information about a specific category 
     * @param  integer $catId     the id of the category
     * @param  any $metaData  if present, the function will return the category metadata, related items, tags, custom fields, galleries and file galleries
     * @param  any $justCount if present, the function will only return the number of related items for each category metadata
     * @return json
     */
    public static function getCategory( $catId, $metaData = null, $justCount = null )
    {
        $action = 'getCategory';
        $data = array( 'catId' => $catId );
        if(isset($metaData)) $data = array_merge($data, array('metaData' => 1));
        if(isset($justCount)) $data = array_merge($data, array('justCount' => 1));
        return self::response( $action, $data );
    }

    /**
     * This function returns the tags associated with the subcategories of a category.
     * @param  integer $catId the id of the Category
     * @return json
     */
    public static function getAllTagsForCategory( $catId )
    {
        $action = 'getAllTagsForCategory';
        $data = array('catId' => $catId);
        return self::response($action, $data);
    }

    /**
     * This function searches for subcategories up to a certain hierarchical level. Subcategories that contain the search term in their names will be returned, or subcategories associated with a tag that contains the searched term.
     * @param  integer $catId      the ID of the parent category
     * @param  integer $level     the maximum level of the child category
     * @param  string $search    the search term
     * @param  any $metaData  if present, the function will return the category metadata, related items, tags, custom fields, galleries and file galleries
     * @param  any $justCount  if present, the function will only return the number of related items for each category metadata
     * @return json
     */
    public static function searchSubcategoriesByLevel( $catId, $level, $search, $metaData = null, $justCount = null )
    {
        $action = 'searchSubcategoriesByLevel';
        $data = array('catId' => $catId, 'level' => $level, 'search' => $search);
        if(isset($metaData)) $data = array_merge($data, array('metaData' => 1));
        if(isset($justCount)) $data = array_merge($data, array('justCount' => 1));
        return self::response( $action, $data );
    }

    /**
     * You can add comments to a category metadata using this function. 
     * @param integer $catId the ID of the category
     * @param string $text  the comment text
     * @return  json
     */
    public static function addCategoryComment( $catId, $text, $custom_udid = null )
    {
        $action = 'addCategoryComment';
        $udid = '';
        if( isset($custom_udid) )
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        $data = array( 'catId' => $catId, 'text' => $text, 'udid' => $udid );
        return self::response( $action, $data );
    }

    /**
     * This function returns the comments associated with an item. 
     * @param  integer $catId    the ID of the category
     * @param  any $extended if present, the function will return the category extended
     * @return json
     */
    public static function getCategoryComments( $catId, $extended = null )
    {
        $action = 'getCategoryComments';
        $data = array('catId' => $catId);
        if(isset($extended)) $data = array_merge($data, array('extended' => 1));
        return self::response($action, $data);
    }

    /**
     * This function can be used for rating an item.
     * @param integer $catId    the ID of the category
     * @param integer $rating the rating of the item
     * @return  json
     */
    public static function addCategoryRating( $catId, $rating, $custom_udid = null )
    {
        if(is_int($rating))
        {
            $action = 'addCategoryRating';
            $udid = '';
            if( isset($custom_udid) )
            {
                $udid = $custom_udid;
            } else {
                $udid = Device::udid();
            }
            $data = array( 'catId' => $catId, 'rating' => $rating, 'udid' => $udid );
            return self::response($action, $data);
        }
    }

    /**
     * This function will return the average rating of an item and the number of ratings submitted 
     * @param  integer $catId    The id of the category
     * @param  any $extended  If present the function will return the extended parametres of the category
     * @return json
     */
    public static function getCategoryRatings( $catId, $extended = null )
    {
        $action = 'getCategoryRatings';
        $data = array( 'catId' => $catId );
        if(isset($extended)) $data = array_merge($data, array('extended' => 1));
        return self::response($action, $data);
    }

          ////////////////////////////////
         //                            //
        // CMS USERS  RELATED HELPERS //
       //                            //
      //////////////////////////////// 

    /**
     * This function returns all data related to the registered users of the app.
     * @return json
     */
    public static function getUsers( $data = null)
    {
        $action = 'getUsers';
        return self::response( $action, $data );
    }

    /**
     * This function returns all data related to a specific user of the app, including all related items.
     * @param  integer $idAppUser the Id of the User
     * @return json
     */
    public static function getUser( $idAppUser )
    {
        $action = 'getUser';
        $data = array( 'idAppUser' => $idAppUser );
        return self::response( $action, $data );
    }

    /**
     * This function returns all custom fields for a user selected by the udid of the associated device.
     * @return json
     */
    public static function getUserfields( $custom_udid = null )
    {
        $action = 'getUserfields';

        $udid = Device::udid();
        if( isset($custom_udid) )
        {
            $udid = $custom_udid;
        }

        $data = array( 'udid' => $udid );
        return self::response( $action, $data );
    }

    /**
     * This function sets the value of the custom field of an user.
     * @param string $fieldName  sets the name of the field
     * @param string $fieldValue sets the value of the field
     * @return json
     */
    public static function setUserfield( $fieldName, $fieldValue, $custom_udid = null )
    {
        $action = 'setUserfield';
        $udid = '';
        if( isset($custom_udid) ) 
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        $data = array('udid' => $udid, 'fieldName' => $fieldName, 'fieldValue' => $fieldValue);
        return self::response( $action, $data );
    }

    /**
     * This function sets a number of custom fields for a user selected by the udid of the associated device.
     * @return json
     */
    public static function setUserfields( $customFields, $custom_udid = null )
    {
        $action = 'setUserfields';
        $data = array();
        $udid = '';
        if( isset($custom_udid) ) 
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        $data = array('udid' => $udid );
        if(is_array($customFields) || is_object($customFields))
        {
            $data = array_merge( $data, array( 'customFields' => json_encode($customFields) ) );
        }
        return self::response( $action, $data );
    }

    /**
     * This function adds a new custom item field for the users of the app. The field type is an integer with values 1- text box 2- dropdown 3- hidden custom field (accessible only from the CMS).
     * @param string $fieldName  sets the name of the field
     * @param string $fieldValue sets the value of the field
     * @return json
     */
    public static function defineUserfield( $fieldName, $fieldValue )
    {
        $continue = true;
        $action = 'defineUserfield';
        switch ($fieldValue) {
            case 'textbox':
                $fieldValue = 1;
                break;
            case 'dropdown':
                $fieldValue = 2;
                break;
            case 'hidden':
                $fieldValue = 3;
                break;
            default:
                if(in_array($fieldValue, array(1, 2, 3)))
                {
                    $continue = false;
                }
                break;
        }
        if($continue)
        {
            $data = array( 'fieldName' => $fieldName, 'fieldType' => $fieldValue );
            return self::response( $action, $data );
        }
    }

    /**
     * This function will delete the value of a custom field for an app user.
     * @param  string $fieldName The name of the custom field to unset is value
     * @return json
     */
    public static function unsetUserfield( $fieldName, $custom_udid = null )
    {
        $udid = '';
        if( isset($custom_udid) )
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        $action = 'unsetUserfield';
        $data = array('udid' => $udid, 'fieldName' => $fieldName);
        return self::response( $action, $data );
    }

    /**
     * This function deletes a custom field of a User from the database.
     * @param  string $fieldName the name of the custom field to be deleted 
     * @return json
     */
    public static function undefineUserfield( $fieldName )
    {
        $action = 'undefineUserfield';
        $data = array('fieldName' => $fieldName);
        return self::response( $action, $data );
    }

    /**
     * This function lists the defined custom fields for app users.
     * @return json
     */
    public static function listUserfields()
    {
        $action = 'listUserfields';
        return self::response( $action );
    }

    /**
     * This function returns the Apple Push Notification Token for a specific udid registered with the app. 
     * @return json
     */
    public static function getPushToken( $custom_udid = null )
    {
        $action = 'getPushToken';
        $udid = '';
        if( isset($custom_udid) )
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        
        $data = array('udid' => $udid);
        return self::response( $action, $data );   
    }

    /**
     * This function will schedule the sending of a push notification message to all the users of the app
     * @param  string $text the text of the notification, base64 encoded
     * @param  date $date the date to schedule the notification for (format YYYY-MM-DD HH:MM:SS)
     * @return json
     */
    public static function sendPushNotification( $text, $date )
    {
        $action = 'sendPushNotification';
        $data = array( 'text' => $text, 'date' => base64_encode($date));
        return self::response( $action, $data );   
    }

    /**
     * This function creates a user in the app and associates a password for login functionality. If an user with the submitted email address exists, the function sets a new password for the user.
     * @param  string $email     email address associated with the user. Will act as the username for logging in.
     * @param  string $password password string. Will be stored in clear, but will only be used for comparison purposes, so it can be sent as a hash string.
     * @param  string $udid     Optional - Device udid, get it by using class Device (Device::udid());
     * @return json
     */
    public static function registerUser( $email, $password, $custom_udid = null )
    {
        $action = 'registerUser';
        $udid = '';
        if( isset($custom_udid) )
        {
            $udid = $custom_udid;
        } else {
            $udid = Device::udid();
        }
        $data = array( 'email' => $email, 'password' => $password);
        $data = array_merge($data, array('udid' => $udid));
        return self::response( $action, $data );
    }

    /**
     * This function helps with the authentication process. The function will return status: OK! if a email/password combination is correct.
     * @param  string $email    self-explanatory
     * @param  string $password self-explanatory
     * @return json
     */
    public static function login($email, $password)
    {
        $action = 'login';
        $data = array( 'email' => $email, 'password' => $password);
        $response = self::response( $action, $data );
        if( sizeof( $response ) > 1 )
        {
            return $response;
        } else {
            return false;
        }
    }

    /**
     * This function sets an existing users details. The accepted fields are the primary fields of an user.
     * @param integer $idAppUser the id of the User registered
     * @param array $data      array containing key->value where key is what to edit and value the value of what you want to edit
     * @return  json
     */
    public static function setUserDetails( $idAppUser, $data )
    {
        $action = 'setUserDetails';
        $data = self::objectToArray($data);
        if(is_array($data))
        {
            $data = array_merge($data, array('idAppUser' => $idAppUser));
            return self::response( $action, $data );
        } else {
            return 'Second parameter is invalid';
        }
    }

    /**
     * This function relates an user with a CMS item through a relation type. 
     * @param  integer $idAppUser the id of the APP
     * @param  integer $itemId    the id of an item
     * @param  integer $rtId      the relation id
     * @return json
     */
    public static function relateUser( $idAppUser, $itemId, $rtId )
    {
        $action = 'relateUser';
        $data = array('idAppUser' => $idAppUser, 'itemId' => $itemId, 'rtId' => $rtId);
        return self::response( $action, $data );
    }

    /**
     * This function deletes a relation between a user and a CMS item.
     * @param  integer $idAppUser the id of the APP
     * @param  integer $itemId    the id of an item
     * @param  integer $rtId      the relation id
     * @return json
     */
    public static function unrelateUser( $idAppUser, $itemId, $rtId )
    {
        $action = 'unrelateUser';
        $data = array('idAppUser' => $idAppUser, 'itemId' => $itemId, 'rtId' => $rtId);
        return self::response( $action, $data );   
    }

          ////////////////////////////////
         //                            //
        // Cloud Services API HELPERS //
       //                            //
      //////////////////////////////// 

    public static function sendMail( $to, $subject, $message )
    {
        $action = 'sendMail';
        $data = array( 'to' => $to, 'subject' => $subject, 'contents' => $message );
        return self::response( $action, $data );
    }

    public static function getInstalls()
    {
        $action = 'getInstalls';
        return self::response( $action );
    }

    public static function getActivityStream()
    {
        $action = 'getActivityStream';
        return self::response( $action );
    }

    public static function trackPageView( $data )
    {
        $action = 'trackPageView';
        return self::response( $action, $data );   
    }

    ///// PRIVATE FUNCTIONS
    
    private static function response ( $action, $data = null )
    {
        $url = self::buildRequest( $action );
        if(!isset( $data )) $data = array();
        if( self::$_isGzipped )
        {
           $data = array_merge($data, array('gzipEnabled' => 1));
        }
        $no_data = sizeof($data);
        if($no_data !== 0) {
            $fields_string = http_build_query($data);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::$_endPointURL.$url);
            curl_setopt($ch, CURLOPT_POST, $no_data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if( self::$_isGzipped )
            {
                $response = gzinflate($response);
            }
            curl_close($ch);
        } else {
            $response = file_get_contents( self::$_endPointURL.$url );
        }

        $response = json_decode($response, true);

        if( is_array($response) && ( \System\Dolphine\Registry::getSetting('api_original') != 'yes' ) )
        {
            $status = 'ERROR';
            if( isset($response['status']) )
            {
                $status = $response['status'];
                unset( $response['status'] );
            }
            $keys = array_keys($response);
            $values = array_values($response);
            $count = sizeof($keys);
            $final_response = new \stdClass;
            if( $count > 2 )
            {          
                for( $i = 0; $i < $count; $i += 1)
                {
                    $final_response->$keys[$i] = $values[$i];
                }
                return $final_response;
            }
            if( $count == 2 )
            {
                $final_raspuns->$keys[0] = $values[0];
                $final_raspuns->$keys[1] = $values[1];
                return $final_response;
            }
            if( $count == 1){
                return $values[0];
            } else {
                return $status;
            }
        } else {
            return $response;
        }

    }

    private static function buildRequest( $action )
    {
        $timestamp = time();
        $_request = '?act='.$action.'&appId='.self::$_appId.'&timestamp='.$timestamp;
        $signature = hash_hmac( 'sha1', $_request , self::$_key );
        $_request .= '&signature='.$signature;
        return $_request;
    }

    private static function objectToArray( $object )
    {
        if( !is_object( $object ) && !is_array( $object ) )
        {
            return $object;
        }
        if( is_object( $object ) )
        {
            $object = get_object_vars( $object );
        }
        return array_map( 'API::objectToArray', $object );
    }

}