<?php
namespace System\Dolphine\Helper;
class HelperAbstract
{
	public function __construct(){}

	public static function base_url( $url = null )
	{
		if(!isset($url)) $url = '';
		return \System\Dolphine\Registry::getSetting('base_url').$url;
	}

	public static function public_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('public_url').$image;
	}

	public static function local_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('local_url').$image;
	}

	public static function cms_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('cmsbaseURL').$image;
	}

	public static function relative_path( $file = null )
	{
		return \System\Dolphine\Registry::getSetting('d_relative_path').$file;
	}

	public static function params( $name = null , $value = null )
	{
		$params = \System\Dolphine\Registry::getDeposit();
		if(isset( $name ))
		{
			if(isset( $params[$name] )){
				return $params[$name];
			} else {
				if(isset($value))
				{
					$params[$name] = $value;
					\System\Dolphine\Registry::setData( $name, $value );
					return $value;
				} else {
					return 'Parameter '.$name.' does not exist';
				}
			}
		}
		return $params;
	}

}