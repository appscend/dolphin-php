<?php
namespace System\Dolphine\Helper;
use \Device as Device;
class APIv2
{
    private static $_appId = null;
    private static $_key = null;
    private static $_instance;
    private static $_isGzipped;
    private static $_endPointURL = 'https://interface.appcend.com/apiv1.php';

  /**
     * [init description]
     * @return [type] [description]
     */
    public static function init()
    {
        if (self::$_instance === null)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    /**
     * [setConf description]
     * @param [type] $appId [description]
     * @param [type] $key   [description]
     */
    public static function setConf( $appId = null, $key = null , $gzip = false )
    {
        self::$_appId = $appId;
        self::$_key = $key;
        if(!isset($gzip)) $gzip = false;
        self::$_isGzipped = $gzip;
    }

  ////////////////////
  // API FUNCTIONS
  ////////////////////

  private static function apiCall($action, $custom_query, $set_cache = null, $time_cache = null)
  {
    $data_from_cache = null;
    if($set_cache){
      $data_from_cache = self::getCache( $action.$custom_query );
    }
    if($data_from_cache){
      return $data_from_cache;
    }else{
      $timestamp = time();
      $query = 'appId='.self::$_appId.'&timestamp='.$timestamp.$custom_query;
      $url = 'https://dev.appscend.net/api/v2/'.$action;
      $token = openssl_digest(self::$_key.$timestamp, 'sha512');
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Token token="'.$token.'"', 'Accept-Encoding: deflate' ) );
      $result = curl_exec($ch);
      $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      //$http_status = 500;
      if(intval($http_status) >= 400){
        $result = new stdClass();
        $result->status = 500;
      }else{
        $result = gzinflate($result);
        $result = json_decode($result);
      }
      if($set_cache){
        self::setCache($action.$custom_query, $result, $time_cache );
      }
      return $result;
    }
  }

  private static function setCache( $key, $data, $time = 900 )
  {
    if(\System\Dolphine\Registry::getSetting('master_api_cache') != "off"){
      ApiCache::setKey($key);
      ApiCache::setCachedData( $data, $time );
    } else {
      return false;
    }
  }

  private static function getCache( $key )
  {
    if(\System\Dolphine\Registry::getSetting('master_api_cache') != "off"){
      ApiCache::setKey($key);
      return ApiCache::getCachedData();
    } else {
      return false;
    }
  }

  ////////////////////
  // GET FUNCTIONS
  ////////////////////

  // CATEGORIES
  public static function getCategories($cache = true)
  {
    $categories = $this->getCategoriesForParent($this->CATEGORIES_CAT, $cache);
    foreach($categories->content as $category)
    {
      $line['catID'] = $category->categoryId;
      $line['title'] = $category->name;
      if($category->metadata->coverPhoto != '')
        $line['coverPhoto'] = $category->metadata->coverPhoto;
      else
        $line['coverPhoto'] = 'http://roxana.freshweb.ro/cardShop/Phone/cards/default.png';
      $result[] = $line;
    }
    return $result;
  }

  public static function getCategoriesForParent($parentId, $cache = true)
  {
    $action = 'category/getCategoriesForParent';
    $custom_query = '&parentId='.$parentId;
    $categories = self::apiCall($action, $custom_query, $cache);
    return $categories;
  }

  //ITEMS
  public static function getItem($itemId, $cache = true)
  {
    $action = 'item/getItem';
    $custom_query = '&itemId='.$itemId;
    $items = self::apiCall($action, $custom_query, $cache);
    return $items;
  }

  public static function getItems($categoryId, $orderBy = null, $orderDir = null, $loadNestedItems = null, $filters = null, $limit = null, $cache = true)
  {
    $action = 'item/getItems';
    $custom_query = '&categoryId='.$categoryId;
    if(isset($orderBy)){
      $custom_query .= '&orderBy='.$orderBy;
    }
    if(isset($orderDir)){
      $custom_query .= '&orderDir='.$orderDir;
    }
    if(isset($loadNestedItems)){
      $custom_query .= '&loadNestedItems='.$loadNestedItems;
    }
    if(isset($filters)){
      $custom_query .= '&filters='.$filters;
    }
    if(isset($limit)){
      $custom_query .= '&limit='.intval($limit);
    }
    $items = self::apiCall($action, $custom_query, $cache);
    return $items;
  }

  //USERS
  public static function getUserByFid($fid, $cache = false)
  {
    $action = 'user/search';
    $custom_query = '&fid='.$fid;
    return self::apiCall($action, $custom_query, $cache);
  }

  //COMMENTS
  public static function getComments($itemId, $extended, $limit,  $cache = true)
  {
    $action = 'item/getComments';
    $custom_query = '&itemId='.$itemId.'&extended='.$extended.'&limit='.$limit;
    return self::apiCall($action, $custom_query, $cache, 10);
  }

  public static function sendEmail($to, $subject, $contents, $cache = false)
  {
    $action = 'app/sendMail';
    $custom_query = '&to='.$to.'&subject='.$subject.'&contents='.$contents;
    return self::apiCall($action, $custom_query, $cache);
  }

  ////////////////////
  // SET FUNCTIONS
  ////////////////////

  public static function setUserfield($idAppUser, $customFields, $cache = false)
  {
    $action = 'user/setUser';
    $custom_query = '&idAppUser='.$idAppUser.'&customFields='.urlencode(json_encode($customFields));
    return self::apiCall($action, $custom_query, $cache);
  }
  public static function setUserUDID($idAppUser, $udid, $cache = false)
  {
    $action = 'user/setUser';
    $custom_query = '&idAppUser='.$idAppUser.'&udid='.$udid;
    return self::apiCall($action, $custom_query, $cache);
  }

  public static function addComment($text, $udid, $itemId, $cache = false)
  {
    $action = 'item/addComment';
    $custom_query = '&text='.$text.'&udid='.$udid.'&itemId='.$itemId;
    return self::apiCall($action, $custom_query, $cache);
  }
}