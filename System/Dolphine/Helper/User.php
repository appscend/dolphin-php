<?php
namespace System\Dolphine\Helper;
interface User_interface
{
	public static function isLogged();
	public static function getName();
	public static function getGender();
	public static function getEmail();
	public static function getImage();
	public static function getFacebookId();
	public static function getFacebookUrl();
	public static function getTwitterId();
	public static function getTwitterUrl();
}
class User implements User_interface
{
	public static function isLogged()
	{
		$user_data = json_decode($_POST['udata'], true);
		if(isset($user_data['logged']))
		{
			if($user_data['logged'] == 'yes')
			{
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static function getImage()
	{
		return self::getData('pic');
	}

	public static function getName()
	{
		return self::getData('name');
	}

	public static function getFacebookUrl()
	{
		return self::getData('furl');
	}

	public static function getGender()
	{
		return self::getData('gender');
	}

	public static function getTwitterUrl()
	{
		return self::getData('turl');
	}

	public static function getEmail()
	{
		return self::getData('e');
	}

	public static function getFacebookId()
	{
		return self::getData('fid');
	}

	public static function getTwitterId()
	{
		return self::getData('tid');
	}

	private static function getData( $data )
	{
		if(isset($_POST['udata'])){
			$user_data = json_decode($_POST['udata'], true);
			if(isset($user_data[$data]))
			{
				return mysql_real_escape_string($user_data[$data]);
			}
		}
	}
}