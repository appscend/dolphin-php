<?php
namespace System\Dolphine\Helper;
class viewHelper
{
	/**
	 * transforms color and alpha to rgba
	 * @param  string $color rgb hex color
	 * @param  int $alpha [description]
	 * @return hex        rgba color
	 */
	public static function color( $color, $alpha = null )
	{
		if(is_int($alpha))
		{
			$alpha = str_pad( dechex( 255*$alpha/100 ), 2, 0, STR_PAD_LEFT );
		} else {
			$alpha = '';
		}
		
		if( $alpha == 0 ) $alpha = '00';

		if(strlen($color) != 6)
		{
			return '000000';
		}
		return $color.''.$alpha;
	}

	/**
	 * return 'yes' or 'no'
	 * @param  bool $bool
	 * @return string
	 */
	public static function boolToLogic( $bool )
	{
		if(is_bool($bool))
		{
			$rtt =  ($bool ? 'yes' : 'no');
			return $rtt;
		} else {
			return $bool;
		}
	}
}