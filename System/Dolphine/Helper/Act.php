<?php
namespace System\Dolphine\Helper;
use \DolphinError as Error;
class Act
{
	/**
	 * [genRef description]
	 * @param  [type]  $avi   [description]
	 * @param  [type]  $array [description]
	 * @param  integer $d     [description]
	 * @return [type]         [description]
	 */
    public static function genRef($avi, $array , $d = null)
	{
		$params = array();
		$errors = 0;
		$r = array(
			'avi' => $avi,
			'x' => 'same',
			'y' => 'same',
			'w' => 'same',
			'h' => 'same',
			'lx' => 'same',
			'ly' => 'same',
			'lw' => 'same',
			'lh' => 'same',
			'padx' => 'same',
			'pady' => 'same',
			'padw' => 'same',
			'padh' => 'same',
			'padlx' => 'same',
			'padly' => 'same',
			'padlw' => 'same',
			'padlh' => 'same',
			'd' => $d,
		);
		if(is_array($array)){
			foreach($array as $key=>$value)
			{
				if(isset($r[$key]))
				{
					$r[$key] = $value;
				} else {
					array_push($params, $key);
					$errors = 1;
				}
			}
		}
		$pttern = $r['avi'].'::'.$r['x'].'::'.$r['y'].'::'.$r['w'].'::'.$r['h'].'::'.$r['lx'].'::'.$r['ly'].'::'.$r['lw'].'::'.$r['lh'].'::'.$r['padx'].'::'.$r['pady'].'::'.$r['padw'].'::'.$r['padh'].'::'.$r['padlx'].'::'.$r['padly'].'::'.$r['padlw'].'::'.$r['padlh'].'::'.$r['d'];
		if($errors == 1){
			$msg_error = 'This parametres are not allowed for this action : ';
			for($i = 0; $i < sizeof($params); $i += 1){
				$msg_error .= $params[$i];
			}
			Error::writeError($msg_error);
		}
		return $pttern;
	}

	/**
	 * [reFrame description]
	 * @param  [type]  $avi   [description]
	 * @param  [type]  $array [description]
	 * @param  integer $d     [description]
	 * @param  [type]  $tavi  [description]
	 * @param  [type]  $k     [description]
	 * @return [type]         [description]
	 */
	public static function reFrame( $avi, $array, $d = 0, $tavi = null, $del = null, $k = null )
	{
		$action = null;
		$params = self::genRef( $avi, $array , $d);
		if(isset($k)){
			$action = array(
				'a' => 'refk:',
				'pr' => $params,
				'tavi' => $tavi,
				'del' => $del,
			);
		} else {
			$action = array(
				'a' => 'ref:',
				'pr' => $params,
				'tavi' => $tavi,
				'del' => $del,
			);
		}
		return $action;
	}
	/**
	 * [genAlpha description]
	 * @param  [type] $avi      [description]
	 * @param  [type] $alpha    [description]
	 * @param  [type] $duration [description]
	 * @return [type]           [description]
	 */
	public static function genAlpha( $avi, $alpha, $duration )
	{
		if(! is_array($alpha)){
			return $avi.'::'.$alpha.'::'.$alpha.'::'.$alpha.'::'.$alpha.'::'.$duration;
		} else {
			return $avi.'::'.$alpha[0].'::'.$alpha[1].'::'.$alpha[2].'::'.$alpha[3].'::'.$duration;
		}
	}

	/**
	 * [hideAvi description]
	 * @param  [type]  $avi                [description]
	 * @param  [type]  $tavi               [description]
	 * @param  integer $animation_duration [description]
	 * @param  integer $del                [description]
	 * @return [type]                      [description]
	 */
	public static function hideAvi( $avi, $tavi = null, $animation_duration = null , $del = null)
	{
		$action = null;
		if(isset($tavi)){
			$action = array(
				'a' => 'rea:',
				'pr' => self::genAlpha( $avi, 0, $animation_duration ),
				'tavi' => $tavi,
				'del' => $del,			
			);
		} else {
			$action = array(
				'a' => 'rea:',
				'pr' => self::genAlpha( $avi, 0, $animation_duration ),	
				'del' => $del,	
			);
		}
		return $action;
	}
	
	/**
	 * [hideAvis description]
	 * @param  [type]  $avi                [description]
	 * @param  [type]  $tavi               [description]
	 * @param  integer $animation_duration [description]
	 * @param  integer $del                [description]
	 * @return [type]                      [description]
	 */
	public static function hideAvis( $avi, $tavi = null, $animation_duration = null , $del = null)
	{
		$action = null;
		if(isset($tavi)){
			$action = array(
				'a' => 'reak:',
				'pr' => self::genAlpha( $avi, 0, $animation_duration ),
				'tavi' => $tavi,
				'del' => $del,
			);
		} else {
			$action = array(
				'a' => 'reak:',
				'pr' => self::genAlpha( $avi, 0, $animation_duration ),
				'del' => $del,	
			);
		}
		return $action;
	}
	
	/**
	 * [showAvi description]
	 * @param  [type]  $avi                [description]
	 * @param  [type]  $tavi               [description]
	 * @param  integer $animation_duration [description]
	 * @param  integer $del                [description]
	 * @return [type]                      [description]
	 */
	public static function showAvi( $avi, $tavi = null, $animation_duration = null, $del = null)
	{
		$action = null;
		if(isset($tavi)){
			$action = array(
				'a' => 'rea:',
				'pr' => self::genAlpha( $avi, 1, $animation_duration ),
				'tavi' => $tavi,
				'del' => $del,
			);
		} else {
			$action = array(
				'a' => 'rea:',
				'pr' => self::genAlpha( $avi, 1, $animation_duration ),
				'del' => $del,
			);
		}
		return $action;
	}
	
	/**
	 * [showAvis description]
	 * @param  [type]  $avi                [description]
	 * @param  [type]  $tavi               [description]
	 * @param  integer $animation_duration [description]
	 * @param  integer $del                [description]
	 * @return [type]                      [description]
	 */
	public static function showAvis( $avi, $tavi = null, $animation_duration = null, $del = null)
	{
		$action = null;
		if(isset($tavi)){
			$action = array(
				'a' => 'reak:',
				'pr' => self::genAlpha( $avi, 1, $animation_duration ),
				'tavi' => $tavi,
				'del' => $del,		
			);
		} else {
			$action = array(
				'a' => 'reak:',
				'pr' => self::genAlpha( $avi, 1, $animation_duration ),
				'del' => $del,		
			);
		}
		return $action;
	}
	/**
	 * [pag description]
	 * @param  [type] $no   [description]
	 * @param  [type] $tavi [description]
	 * @return [type]       [description]
	 */
	public static function pag( $no, $tavi = null, $del = null )
	{
		$action = array(
			'a' => 'pag:',
			'pr' => $no,
			'del' => $del,
			'tavi' => $tavi,
			);
		if(!isset($tavi)){
			unset($action['tavi']);
		}
		return $action;
	}
	/**
	 * [push description]
	 * @param  [type] $avi  [description]
	 * @param  [type] $path [description]
	 * @return [type]       [description]
	 */
	public static function push( $avi, $path, $del = null )
	{
		$action = array(
			'a' => 'p:',
			'pr' => $path,
			'tavi' => $avi,
			'del' => $del,
		); 
		return $action;
	}
	/**
	 * [replace description]
	 * @param  [type] $avi  [description]
	 * @param  [type] $path [description]
	 * @return [type]       [description]
	 */
	public static function replace( $avi, $path, $del = null )
	{
		$action = array(
			'a' => 'r:',
			'pr' => $path,
			'tavi' => $avi,
			'del' => $del,
		); 
		return $action;
	}

	/**
	 * [replaceAll description]
	 * @param  [type] $avi  [description]
	 * @param  [type] $path [description]
	 * @return [type]       [description]
	 */
	public static function replaceAll( $avi, $path, $del = null )
	{
		$action = array(
			'a' => 'ra:',
			'pr' => $path,
			'tavi' => $avi,
			'del' => $del,
		); 
		return $action;
	}
	/**
	 * [changelabelName description]
	 * @param  [type] $avi  [description]
	 * @param  [type] $name [description]
	 * @param  [type] $tavi [description]
	 * @return [type]       [description]
	 */
	public static function changelabelName( $avi, $name, $tavi = null, $del = null )
	{
		return self::rps( $avi, 'n', $name, $tavi, $del );
	}
	public static function changeBackgroundColor( $avi, $color, $tavi = null, $del = null )
	{
		return self::rps( $avi, 'bc', $color, $tavi, $del );
	}
	/**
	 * [rps description]
	 * @param  [type] $avi      [description]
	 * @param  [type] $value    [description]
	 * @param  [type] $property [description]
	 * @param  [type] $tavi     [description]
	 * @param  [type] $del      [description]
	 * @return [type]           [description]
	 */
	public static function rps($avi, $property, $value, $tavi = null , $del = null)
	{
		$action = array(
				'a' => 'rps:',
				'pr' => $avi.'::'.$value.'::'.$property,
				'tavi' => $tavi,
				'del' => $del,
			);
		if(!isset($tavi)){
			unset($action['tavi']);
		}
		if(!isset($del)){
			unset($action['del']);
		}
		return $action;
	}
	/**
	 * [rpsk description]
	 * @param  [type] $avi      [description]
	 * @param  [type] $value    [description]
	 * @param  [type] $property [description]
	 * @param  [type] $tavi     [description]
	 * @param  [type] $del      [description]
	 * @return [type]           [description]
	 */
	public static function rpsk($avi, $property, $value, $tavi = null , $del = null)
	{
		$action = array(
				'a' => 'rpsk:',
				'pr' => $avi.'::'.$value.'::'.$property,
				'tavi' => $tavi,
				'del' => $del,
			);
		if(!isset($tavi)){
			unset($action['tavi']);
		}
		if(!isset($del)){
			unset($action['del']);
		}
		return $action;
	}

	/**
	 * [path_and_data description]
	 * @param  [type] $path [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public static function path( $path , $data = null , $form = null, $animation = null)
	{
		if(is_array($data) || is_object($data))
		{
			$data = json_encode($data);
		}

		$path = $path."::".$data;
		
		if(isset($form))
		{
			$path = $path."::".$form;
		} else {
			$path = $path."::";
		}
		if(isset($animation))
		{
			$path = $path."::".$animation;	
		}
		return $path;
	}

	public static function alert( $msg = null )
	{
		if(isset($msg))
		{
			return array(
				'a' => 'alert:',
				'pr' => $msg,
				);
		}
	}
	public static function color( $color, $alpha = null )
	{
		if(is_int($alpha))
		{
			$alpha = str_pad( dechex( 255*$alpha/100 ), 2, 0, STR_PAD_LEFT );
		} else {
			$alpha = '';
		}

		if($alpha == 0) $alpha = '00';
		
		if(strlen($color) != 6)
		{
			return '000000';
		}
		return $color.''.$alpha;
	}

	/**
	 * [orderByLetters description]
	 * @param  [type] $items [description]
	 * @return [type]        [description]
	 */
	public static function orderByLetters( $items ){
		$array_letters = array();
		for($i = 0; $i < sizeof($items); $i+=1)
		{
			$checkLetter = $items[$i]->title[0];
				$k = 0;
				for($j = 0; $j < sizeof($items); $j+=1)
				{
					if($checkLetter == $items[$j]->title[0])
					{
						$array_letters[$checkLetter][$k] = $items[$j];
						$k += 1;
					}
				}
				$array_letters[$checkLetter]['no'] = $k;
		}	
		return $array_letters;
	}

	private static function objectToArray( $object )
    {
        if( !is_object( $object ) && !is_array( $object ) )
        {
            return $object;
        }
        if( is_object( $object ) )
        {
            $object = get_object_vars( $object );
        }
        return array_map( 'Act::objectToArray', $object );
    }

    /**
	 * return 'yes' or 'no'
	 * @param  bool $bool
	 * @return string
	 */
	public static function boolToLogic( $bool )
	{
		if(is_bool($bool))
		{
			$rtt =  ($bool ? 'yes' : 'no');
			return $rtt;
		} else {
			return $bool;
		}
	}
}