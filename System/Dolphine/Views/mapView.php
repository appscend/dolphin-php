<?php
class mapView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'm');
	}

	public function setMapType( $s )
	{
		if( in_array($s, array(0,1,2)) || in_array($s, array('standard', 'satellite', 'hybrid')) )
		{
			switch( $s )
			{
			case 'standard':
				$s = 0;
				break;
			case 'satellite':
				$s = 1;
				break;
			case 'hybrid' :
				$s = 2;
				break;
			default:
				break;
			}
			$this->addConfig('mt', $s);
		}
	}

	public function setZoomEnabled( $val = true, $valPad = null )
	{
		$this->addConfig( 'ze', $val, $valPad );
	}

	public function setScrollEnabled( $val = true, $valPad = null )
	{
		$this->addConfig( 'se', $val, $valPad );
	}

	public function setInitialLocation( $geoLocation )
	{
		$this->addConfig( 'il', $geoLocation );
	}

	public function setMapSpan( $val )
	{
		$this->addConfig( 'is', $val );
	}

	public function showUserLocation( $val = true )
	{
		$this->addConfig( 'ul', $val );
	}

	public function trackUserLocation( $val = true )
	{
		$this->addConfig( 'tu', $val );
	}

	public function setAccuracyLevel( $val = 0 )
	{
		if(is_int($val))
		{
			$this->addConfig('acr', $val);
		}
	}

	public function setPinImage( $val )
	{
		$this->addConfig('pin', $val );
	}

	public function setFitElements( $val )
	{
		$this->addConfig('fit', $val );
	}

	public function setPinWidth( $val )
	{
		$this->addConfig( 'pisw', $val );
	}

	public function setPinHeight( $val )
	{
		$this->addConfig( 'pish', $val );
	}

	public function setPinRadius( $val )
	{
		$this->addConfig( 'pir', $val );
	}

	public function setPinImageMode( $val )
	{
		$icm = self::checkIfIcmB( $val );
		$this->addConfig( 'picm', $icm );
	}

	public function setImageWidth( $val )
	{
		$this->addConfig('isw', $val );
	}

	public function setImageHeight( $val )
	{
		$this->addConfig('ish', $val );
	}

	public function setImageCornerRadius( $val )
	{
		$this->addConfig('icr', $val );
	}

	public function setClusterEnabled( $val )
	{
		if(is_int($val)){
			$this->addConfig('clg', $val);
			$this->addConfig('cluster', true);
		} else {
			$this->addConfig('cluster', $val);
		}
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

	private function checkIfIcmB( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}
}