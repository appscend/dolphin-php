<?php
class qrScannerView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'cs');
	}

	public function useCustomInfo( $arg, $argPad = null )
	{
		$this->addConfig('cv', $arg, $argPad );
	}

	public function setCustomInfo( $content, $contentPad = null )
	{
		$this->addConfig('content', $content, $contentPad );
	}

	public function autoStartEnable( $arg, $argPad = null )
	{
		$this->addConfig('as', $arg, $argPad );
	}

	public function autoBackEnable( $arg, $argPad = null )
	{
		$this->addConfig('ap', $arg, $argPad );
	}

	public function confirmScan( $arg, $argPad = null )
	{
		$this->addConfig('aa', $arg, $argPad );
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

}