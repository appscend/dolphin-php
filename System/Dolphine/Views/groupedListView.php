<?php
class groupedListView extends listView
{
  public function __construct( $avi = null )
  {
    parent::__construct($avi, 'g');
  }

  public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
  {
    parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
  }
}