<?php
class genericView
{
	public $_config;
	public $_elements;
	public $_menuGroup;
	public $_buttons;
	public $_buttonGroup;
	public $_genericAction;
	public $_javaFunctions;
	public $_actionGroup;
	public $_launchActions;
	public $_tabs;
	public $_favoriteTemplate;
	public $_elementTemplate;
	public $_sections;

	public $_is_list;

	public function __construct( $avi = null )
	{
		self::init();
		if(isset($avi)){
			$this->_config['avi'] = $avi;
		}
	}

	//CONFIG RELATED
	//
	/**
	 * this function adds config parametres and values to your view
	 * @param string $param    name of the config property
	 * @param string $value   value of the config property
	 * @param string $valuePad value of the config property for ipad if
	 */
	public function addConfig( $param, $value , $valuePad = null )
	{
		if(isset($param) && isset($value))
		{
			$this->_config[$param] = viewHelper::boolToLogic($value);
			if(isset($valuePad))
			{
				$param  = "pad".$param;
				$this->_config[$param] = viewHelper::boolToLogic($valuePad);
			}
		}
	}

	/**
	 * sets the View Type of your view
	 * @param string $value see view type codes on : http://appscend.zendesk.com/forums/20720262-ignitemarkup-documentation
	 */
	public function setViewType( $value )
	{
		$this->_config['vt'] = $value;
	}

	/**
	 * Should scrolling inside the content of the widget view be paged (so that a user can only scroll one page at a time)?
	 * @param bool $val
	 * @param bool $valPad
	 */
	public function setPageEnabled( $val, $valPad = null )
	{
		$this->addConfig('pg', $val, $valPad);
	}

	/**
	 * Disable vertical scrolling inside the widget view
	 * @param bool $val
	 * @param bool $valPad
	 */
	public function setDisableVerticalScrolling( $val, $valPad = null  )
	{
		$this->addConfig('dvs', $val, $valPad);
	}

	/**
	 * Disable horizontal scrolling inside the widget view
	 * @param bool $val
	 * @param bool $valPad
	 */
	public function setDisableHorizontalScrolling( $val, $valPad = null  )
	{
		$this->addConfig('dhs', $val, $valPad);
	}

	/**
	 * hide vertical scrolling inside the widget view
	 * @param bool $val
	 * @param bool $valPad
	 */
	public function setHideVerticalScrolling( $val, $valPad = null  )
	{
		$this->addConfig('hvs', $val, $valPad);
	}

	/**
	 * hide horizontal scrolling inside the widget view
	 * @param bool $val
	 * @param bool $valPad
	 */
	public function setHideHorizontalScrolling( $val, $valPad = null  )
	{
		$this->addConfig('hhs', $val, $valPad);
	}

	/**
	 * sets the bacground color of your view
	 * @param  string $color ex: FF0000 - red
	 * @param  integer $alpha 0-100
	 */
	public function backgroundColor( $color , $alpha = null )
	{
		$this->addConfig('bc', viewHelper::color( $color, $alpha));
	}

	public function backgroundImage( $img, $imgPad = null )
	{
		$this->addConfig('bi', $img, $imgPad );
	}

	public function backgroundImageMode( $icm , $icmPad = null, $prefix = null )
	{
		if(!isset($prefix)) $prefix = '';
		$icm = self::checkIfIcm( $icm );
		$icmPad = self::checkIfIcm( $icmPad );
		$this->addConfig( $prefix.'bicm', $icm, $icmPad );
	}

	/**
	 * enables/disables the navigation bar on your view
	 * @param  boolean $value true/false default true
	 */
	public function navigationBar( $value = true )
	{
		if(is_bool($value)){
			$this->addConfig('nbv', $value );
		}
		if(isset($value->_params))
		{
			$this->addConfig('nbv', true );
			foreach($value->_params as $key=>$value)
			{
				$this->addConfig($key, $value);
			}
		}
	}

	/**
	 * enables/disables the toolbar on your view
	 * @param  boolean $value true/false default true
	 */
	public function toolBar( $value = true )
	{
		if(is_bool($value)){
			$this->addConfig('tlv', $value );
		}
		if(isset($value->_params))
		{
			$this->addConfig('tlv', true );
			foreach($value->_params as $key=>$value)
			{
				$this->addConfig($key, $value);
			}
		}
	}

	/**
	 * enables disables tab bar on your view
	 * @param  boolean $value true/false default true
	 */
	public function tabBar( $value = true )
	{
		$this->addConfig( 'tbv', $value );
	}

	/**
	 * AVI - appscend view identifier. Sets the identifier for your view for furture use.
	 * @param  string $value the identifier value
	 */
	public function avi( $value = null )
	{
		if(isset($value)) $this->addConfig( 'avi', $value );
	}

	public function setAvi( $value = null )
	{
		if(isset($value)) $this->addConfig( 'avi', $value );
	}

	/**
	 * Unset all the config properties
	 */
	public function clearConfig()
	{
		self::reset('_config');
	}

	/**
	 * clear all the objects/elements from your view
	 */
	public function emptyView()
	{
		$class_vars = array('_config','_elements','_menuGroup','_buttonGroup', '_genericAction', '_javaFunctions', '_launchActions', '_tabs', '_favoriteTemplate', '_elementTemplate');
		foreach ($class_vars as $name => $value) {
		    	self::reset($value);
		}
	}


	// STRUCTURE
	//
	/**
	 * Add object to $this->_elements by converting them to an array
	 */
	public function addElements()
	{
		self::addSingleLevel( func_get_args(), '_elements' );
	}

	/**
	 * unsets $this->_elements
	 */
	public function resetElements()
	{
		self::reset('_elements');
	}

	/**
	 * Add objects to $this->_tabs by converting them to an array
	 */
	public function addTabs()
	{
		self::addSingleLevel( func_get_args(), '_tabs' );
	}

	/**
	 * unsets $this->_tabs
	 * @return [type] [description]
	 */
	public function resetTabs()
	{
		self::reset('_tabs');
	}

	/**
	 * Add objects to $this->_menuGroup by converting them to an array
	 */
	public function addMenuGroup()
	{
		self::addMultipleLevel( func_get_args(), '_menuGroup' );
	}

	/**
	 * unsets $this->_menuGroup
	 */
	public function resetMenuGroup()
	{
		self::reset('_menuGroup');
	}

	/**
	 * Add objects to $this->_buttons by converting them to an array
	 */
	public function addButtons()
	{
		self::addMultipleLevel( func_get_args(), '_buttons' );
	}

	/**
	 * unsets $this->_buttons
	 */
	public function resetButtons()
	{
		self::reset('_buttons');
	}

	/**
	 * Add object to $this->_buttonGroup by converting them to an array
	 */
	public function addButtonGroup()
	{
		self::addMultipleLevel( func_get_args(), '_buttonGroup' );
	}

	/**
	 * unsets $this->_buttonGroup
	 * @return [type] [description]
	 */
	public function resetButtonGroup()
	{
		self::reset('_buttonGroup');
	}

	/**
	 * Add action objects to $this->_actionGroup by converting them to an array
	 */
	public function addActionGroup()
	{
		$set_of_elements = array();
		$params = func_get_args();
		foreach ($params as $key => $value) {
            if (!is_null($value)) {
            	array_push($set_of_elements, $value);
            }
        }
        if( sizeof($set_of_elements)>0 ){
        	array_push($this->_actionGroup, $set_of_elements);
        }
	}

	/**
	 * unsets $this->_actionGroup
	 * @return [type] [description]
	 */
	public function resetActionGroup()
	{
		self::reset('_actionGroup');
	}

	public function addLaunchActions()
	{
		self::addMultipleLevel( func_get_args(), '_launchActions' );
	}

	public function resetLaunchActions()
	{
		self::reset('_launchActions');
	}

	public function addJSFunctions()
	{
		self::addMultipleLevel( func_get_args(), '_javaFunctions' );
	}

	public function resetJSFunctions()
	{
		self::reset('_javaFunctions');
	}

	public function addGeneralAction( $action = null )
	{
		if(isset($action))
		{
			if(is_object($action))
			{
				$this->_genericAction = $action;
			}
		}
	}

	public function resetGeneralAction()
	{
		self::reset('_genericAction');
	}


	public function addFavoriteTemplate ( $param )
	{
		if(isset($param))
		{
			if(is_object($param))
			{
				$this->_favoriteTemplate = $param;
			}
		}
	}

	public function resetFavoriteTemplate()
	{
		self::reset('_favoriteTemplate');
	}

	public function addElementTemplate ( $param )
	{
		self::addMultipleLevel( func_get_args(), '_elementTemplate' );
	}

	public function resetElementTemplate()
	{
		self::reset('_elementTemplate');
	}

	public function addTemplateElement ( $param )
	{
		self::addMultipleLevel( func_get_args(), '_elementTemplate' );
	}

	public function resetTemplateElement()
	{
		self::reset('_elementTemplate');
	}

	// GENERATE VIEW
	//
	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null )
	{

		$master = array();

		if(isset($this->_config))
		{
			if( sizeof($this->_config) > 0 ){
				$master['cfg'] = array();
				array_push($master['cfg'], $this->_config);
			}
		}

		if(isset($this->_tabs) && sizeof($this->_tabs)>0 )
		{
			for($i = 0; $i<sizeof($this->_tabs); $i+=1)
			{
				array_push($master, array('tab' => $this->_tabs[$i]->_params));
			}
		}

		if(!$this->_is_list){
			if( sizeof($this->_elements) > 0 ){
				$master['s']['es'] = array();
				$e = array();
				foreach($this->_elements as $key =>$value )
				{
					array_push($e, array('e' => $value->_params ) );
				}
				if( sizeof($e) > 0)
				{
					array_push($master['s']['es'], $e);
				} else {
					$master['s']['es'] = "";
				}
			}
		} else {
			foreach ($this->_sections as $key => $value) {
				$section = array();
				array_push($section, $value['s']['cfg']);

					$section['es'] = array();
					foreach($value['s']['es'] as $holder => $e )
					{
						array_push($section['es'], array('e' => $e['e']));
					}
				array_push($master, array('s' => $section ));
			}
		}

		if(isset($this->_genericAction->_params))
		{
			$master['ga'] = $this->_genericAction->_params;
		}

		if(isset($this->_actionGroup) && sizeof($this->_actionGroup)>0)
		{
			$master['ags'] = array();
			for($i = 0; $i<sizeof($this->_actionGroup); $i+=1)
			{
				$ag = array('agn'=>null);
				for($j = 0; $j<sizeof($this->_actionGroup[$i]); $j+=1)
				{
					if(is_string($this->_actionGroup[$i][$j])){
						$ag['agn'] = $this->_actionGroup[$i][$j];
					}else{
						array_push($ag, array('age' => $this->_actionGroup[$i][$j]->_params));
					}
				}
				if($ag['agn'] == null ) unset($ag['agn']);
				array_push($master['ags'], array('ag' => $ag));
			}
		}
		if(isset($this->_buttons) && sizeof($this->_buttons)>0)
		{
			if(!isset($master['bs'])) $master['bs'] = array();
			for($j = 0; $j<sizeof($this->_buttons); $j+=1)
			{
				for($i = 0; $i<sizeof($this->_buttons[$j]); $i+=1)
				{
					array_push($master['bs'], array('b' => $this->_buttons[$j][$i]->_params));
				}
			}
		}

		if(isset($this->_buttonGroup) && sizeof($this->_buttonGroup)>0)
		{
			if(!isset($master['bs'])) $master['bs'] = array();
			if( isset($this->_buttonGroup[0])){
				$groups = $this->_buttonGroup[0];
				for($i = 0; $i<sizeof($groups); $i+=1)
				{
					 if(isset($groups[$i]->_buttons)){
					 	$bg = array();
					 	array_push($bg, array($groups[$i]->_params));
					 	for($j = 0; $j<sizeof($groups[$i]->_buttons); $j+=1)
						{
							array_push($bg, array('b' => $groups[$i]->_buttons[$j]->_params));
						}
						array_push($master['bs'], array('bg' => $bg));
					 }
				}
			}
		}
		if(isset($this->_javaFunctions) && sizeof($this->_javaFunctions)>0 )
		{
			$master['funcs'] = array();
			for($i = 0; $i<sizeof($this->_javaFunctions); $i+=1)
			{
					for($j = 0; $j<sizeof($this->_javaFunctions[$i]); $j+=1)
					{
						array_push($master['funcs'], array('func' => $this->_javaFunctions[$i][$j]->_params));
					}
			}
		}
		if(isset($this->_launchActions) && sizeof($this->_launchActions)>0)
		{
			$master['las'] = array();
			for($i = 0; $i<sizeof($this->_launchActions); $i+=1)
			{
					for($j = 0; $j<sizeof($this->_launchActions[$i]); $j+=1)
					{
						array_push($master['las'], array('la' => $this->_launchActions[$i][$j]->_params));
					}
			}
		}
		if(isset($this->_menuGroup) && sizeof($this->_menuGroup)>0 )
		{
			$master['ms'] = array();
			for($i = 0; $i<sizeof($this->_menuGroup); $i+=1)
			{
				$m = array();
					for($j = 0; $j<sizeof($this->_menuGroup[$i]); $j+=1)
					{
						array_push($m, array('me' => $this->_menuGroup[$i][$j]->_params));
					}
				array_push($master['ms'], array('m' => $m));
			}

		}
		if(isset($this->_favoriteTemplate) && sizeof($this->_favoriteTemplate)>0 )
		{
			if(isset($this->_favoriteTemplate->_params))
			{
				if( sizeof($this->_favoriteTemplate->_params) > 0 )
				$master['ef'] = $this->_favoriteTemplate->_params;
			}
		}

		if(isset($this->_elementTemplate) && sizeof($this->_elementTemplate)>0 )
		{
			$master['ets'] = array();
			for($i = 0; $i<sizeof($this->_elementTemplate); $i+=1 ){
				$et = array();
					for( $j = 0; $j<sizeof($this->_elementTemplate[$i]); $j+=1 ){
						array_push($et, array('et' => $this->_elementTemplate[$i][$j]->_params));
					}
				array_push($master['ets'], $et);
			}
		}
		imGen::generateView($master, $gzip, $cache, $device_cache, $cache_time_out );
	}

	//PRIVATE FUNCTION
	//
	private function init(){
		$this->_config = array();
		$this->_elements = array();
		$this->_actionGroup = array();
		$this->_menuGroup = array();
		$this->_launchActions = array();
		$this->_genericAction = array();
		$this->_javaFunctions = array();
		$this->_buttonGroup = array();
		$this->_tabs = array();
		$this->_favoriteTemplate = array();
		$this->_elementTemplate = array();
		$this->_buttons = array();
		$this->_sections = array();
		$this->_is_list = false;
	}

	private function reset( $name )
	{
		$this->$name = array();
	}

	private function checkIfIcm( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}

	public function addSingleLevel( $params , $where )
	{
		foreach ($params as $key => $value) {
            if (!is_null($value)) {
            	if(is_object($value))
            	{
            		array_push($this->$where, $value);
            	}
            }
        }
	}

	public function addMultipleLevel( $params , $where )
	{
		$set_of_elements = array();
		foreach ($params as $key => $value) {
            if (!is_null($value)) {
            	if(is_object($value))
            	{
            		array_push($set_of_elements, $value);
            	}
            }
        }
        if( sizeof($set_of_elements)>0 ){
        	array_push($this->$where, $set_of_elements);
        }
	}

}
