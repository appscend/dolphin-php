<?php
class webView extends genericView
{
	public $content;
	public $content_data;
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'w');
		$this->addConfig('show', 'url');
		$this->content = new element( null );
	}

	private function addPropertyToElement( $prop, $value )
	{
		$this->content->setProperty($prop, $value);
	}

	public function title( $val = null )
	{
		self::addPropertyToElement('title', $val);
		$this->showTitle();
	}

	public function shortDescription( $val = null )
	{
		self::addPropertyToElement('d', $val);
	}

	public function image( $val = null )
	{
		self::addPropertyToElement('i', $val);
		$this->showImage();
	}

	public function largeImage( $val = null )
	{
		self::addPropertyToElement('li', $val);
	}

	public function content( $content = null )
	{
		$this->setType('content');
		$this->content_data = $content;
	}

	public function url( $content = null )
	{
		$this->setType('url');
		$this->content_data = $content;
	}

	public function html( $content = null )
	{
		$this->setType('htmlc');
		$this->content_data = $content;
	}


	public function webContent ( $element )
	{
		$this->addElements( $element );
	}

	public function setType( $val )
	{
		$this->addConfig('show', $val );
	}

	public function showTitle( $val = true )
	{
		$this->addConfig('showt', viewHelper::boolToLogic($val));
	}

	public function showImage( $val = true )
	{
		$this->addConfig('showi', viewHelper::boolToLogic($val));
	}

	public function generateView( $gzip = null , $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		self::addPropertyToElement($this->_config['show'], $this->content_data);
		if($this->_config['show'] != 'url')
		{
			$this->content->unsetProperty('title');
			$this->showTitle(false);
			$this->content->unsetProperty('d');
			$this->content->unsetProperty('i');
			$this->content->unsetProperty('li');
			$this->showImage(false);
		}
		$this->webContent($this->content);
		parent::generateView($gzip, $cache, $device_cache, $cache_time_out );
	}
}