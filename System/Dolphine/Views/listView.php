<?php
class listView extends genericView
{
	public $_sections;
	public function __construct( $avi = null , $type_list = null )
	{
		parent::__construct($avi);
		$this->_is_list = true;
		$this->addConfig('vt', 'l');
		if(isset($type_list))
		{
			$this->addConfig('ls', $type_list);
		}
	}

	public function setListHeader( $obj )
	{
		if(isset($obj->_params))
		{
			foreach ($obj->_params as $key => $value) {
				$this->addConfig($key, $value);
			}
		}
	}

	public function setIndexable( $val, $valPad = null )
	{
		$this->addConfig( 'sid', $val, $valPad );
	}

	public function setCellsConfig( $obj )
	{
		if(isset($obj->_params))
		{
			foreach ($obj->_params as $key => $value) {
				$this->addConfig($key, $value);
			}
		}
	}

	public function setSectionsConfig( $obj )
	{
		if(isset($obj->_params['cfg']))
		{
			foreach ($obj->_params['cfg'] as $key => $value) {
				if($key != 'sn' && $key != 'ssc')
				{
					$this->addConfig($key, $value);
				}
			}
		}
	}

	public function setScrollDisabeld( $val, $valPad = null )
	{
		$this->addConfig('lsd', $val, $valPad);
	}

	public function setRefreshEnabled( $val, $valPad = null )
	{
		$this->addConfig('dr', $val, $valPad);
	}

	public function setRefreshAreaColor( $color, $alpha )
	{
		$this->addConfig('drbc', viewHelper::color( $color, $alpha), null);
	}

	public function setKeepSelected( $val = true , $valPad = true )
	{
		$this->addConfig('ks', $val, $valPad );
	}

	public function addElements()
	{
		foreach (func_get_args() as $key => $obj) {
			if(isset($obj->_params)){
				$s = array( 's' => $obj->_params );
				array_push( $this->_sections, $s );
			}
		}
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

}