<?php
class imageGridView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'p');
	}

	public function setNoColumns( $val, $valPad = null )
	{
		$this->addConfig('c', $val, $valPad);
	}

	public function setNoRows( $val, $valPad = null )
	{
		$this->addConfig('r', $val, $valPad);
	}

	public function setAutoRows()
	{
		$this->addConfig('r', 0, 0);
	}

	public function setTopPadding( $val, $valPad = null )
	{
		$this->addConfig('tp', $val, $valPad );
	}

	public function setVerticalPadding( $val, $valPad = null )
	{
		$this->addConfig('vp', $val, $valPad );
	}

	public function setPageToShow( $val, $valPad = null )
	{
		$this->addConfig('p', $val, $valPad );
	}

	public function setAutoHideNavigationBar( $val, $valPad = null )
	{
		$this->addConfig('ahnb', $val, $valPad );
	}

	public function setTextShowEnabled( $val, $auto = null )
	{
		$this->addConfig('sl', $val, null );
		if(isset($auto)){
			$this->addConfig('npo', true);
		}
	}

	public function setImageCornerRadius( $val, $valPad = null )
	{
		$this->addConfig('icr', $val, $valPad );
	}

	public function setImageWidth( $val, $valPad = null )
	{
		$this->addConfig('isw', $val, $valPad );
	}

	public function setImageHeight( $val, $valPad = null )
	{
		$this->addConfig('ish', $val, $valPad );
	}

	public function setImageDisplayMode( $val, $valPad = null )
	{
		$icm = self::checkIfIcmB( $val );
		$icmPad = self::checkIfIcmB( $valPad );
		$this->addConfig( 'icm', $icm, $icmPad );
	}

	public function setFont()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if( $no_args > 0 ) $this->addConfig( 'nf', $args[0], null );
		if( $no_args > 1 ) $this->addConfig( 'ns', $args[1], null );
		if( $no_args > 2 ) $this->addConfig( 'nc', $args[2], null );

	}

	public function setNoLines( $val, $valPad = null )
	{
		$this->addConfig( 'nl', $val, $valPad );
	}

	public function setFontSize( $val, $valPad = null )
	{
		$this->addConfig( 'ns', $val, $valPad );
	}

	public function setFontColor( $val, $valPad = null )
	{
		$this->addConfig( 'nc', $val, $valPad );
	}

	public function setTextPadding( $val, $valPad = null )
	{
		$this->addConfig('np', $val, $valPad );
	}

	public function setTextBackgroundColor( $color, $alpha = null )
	{
		$this->addConfig('nbc',  viewHelper::color( $color, $alpha ));
	}

	public function setTextAlignment( $val, $valPad = null )
	{
		$a = self::checkIfAlignment( $val );
		$b = self::checkIfAlignment( $valPad );
		$this->addConfig( 'na', $a, $b );
	}

	public function setImagePlaceholder( $img , $imgPad = null )
	{
		$bleah = null;
		if(isset($imgPad)) $bleah = true;
		$this->addConfig( 'splh', true , $bleah );
		$this->addConfig( 'plh', $img, $imgPad );
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

	private function checkIfIcmB( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}

	private function checkIfAlignment( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'r', 'c', 'l', 'right', 'center', 'left' )))
		{
			switch( $val )
			{
				case 'right' :
					return 'r';
					break;
				case 'center' :
					return 'c';
					break;
				case 'left' :
					return 'l';
					break;
				default :
					return $val;
					break;
			}
		} else {
			return null;
		}
	}

}