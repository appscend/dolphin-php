<?php
class menuBarView extends genericView
{

	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'mb');
	}

	public function setMenuHeight( $height, $heightPad = null )
	{
		$this->addConfig('mbh', $height, $heightPad );
	}

	public function setMenuBgColor( $color , $alpha = null )
	{
		$this->addConfig('mbbc', viewHelper::color( $color, $alpha));
	}

	public function setMenuBImage( $img, $imgPad = null )
	{
		$this->addConfig('mbbi', $img, $imgPad );
	}

	public function setMenuBImageMode( $icm, $icmPad = null )
	{
		$this->backgroundImageMode( $icm, $icmPad , 'mb' );
	}

	public function setSelectedColor( $color, $alpha = null )
	{
		$this->addConfig('mbsc', viewHelper::color( $color, $alpha));
	}

	public function setSelectedShadowColor( $color, $alpha = null )
	{
		$this->addConfig('mbssc', viewHelper::color( $color, $alpha));
	}

	public function setUnselectedColor( $color, $alpha = null )
	{
		$this->addConfig('mbuc', viewHelper::color( $color, $alpha));
	}

	public function setUnselectedShadowColor( $color, $alpha = null )
	{
		$this->addConfig('mbusc', viewHelper::color( $color, $alpha));
	}

	public function setSelectedBImage( $img, $imgPad = null )
	{
		$this->addConfig('mbsi', $img, $imgPad );
	}

	public function setLeftPaddingBImage( $val, $valPad = null )
	{
		$lc = 0;
		if(is_int($val)) $lc = $val;
		$lcPad = null;
		if(is_int($valPad)) $lcPad = $valPad;
		$this->addConfig('mbsilc', $lc, $lcPad );
	}

	public function setUnselectedBImage( $img, $imgPad = null )
	{
		$this->addConfig('mbui', $img, $imgPad );
	}

	public function setLeftPaddingUBImage( $val, $valPad = null )
	{
		$lc = 0;
		if(is_int($val)) $lc = $val;
		$lcPad = null;
		if(is_int($valPad)) $lcPad = $valPad;
		$this->addConfig('mbuilc', $lc, $lcPad );
	}

	public function setMenuFont( $val, $valPad = null )
	{
		$this->addConfig( 'mbf', $val, $valPad );
	}

	public function setMenuFontSize( $val, $valPad = null )
	{
		$this->addConfig( 'mbfs', $val, $valPad );
	}

	public function setLeftScrollImage( $img, $imgPad = null )
	{
		$this->addConfig('lsi', $img, $imgPad );
	}

	public function setRightScrollImage( $img, $imgPad = null )
	{
		$this->addConfig('rsi', $img, $imgPad );
	}

	public function setButtonPadding( $val, $valPad = null )
	{
		$this->addConfig('bp', $val, $valPad );
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

}