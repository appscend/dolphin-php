<?php
class widgetView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct( $avi );
		$this->addConfig('vt', 'wd');
	}

	public function setBounceEnable( $val, $valPad = null )
	{
		$this->addConfig('bnc', $val, $valPad);
	}

	public function setXSyncWith( $id, $idPad = null )
	{
		$this->addConfig('syncx', $id, $idPad );
	}

	public function setYSyncWith( $id, $idPad = null )
	{
		$this->addConfig('syncy', $id, $idPad );
	}

	public function setMaxWdith( $val, $valPad = null )
	{
		$this->addConfig('maxw', $val, $valPad );
	}

	public function setMaxHeight( $val, $valPad = null )
	{
		$this->addConfig('maxh', $val, $valPad );
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

}