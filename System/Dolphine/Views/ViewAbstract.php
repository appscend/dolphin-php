<?php
namespace System\Dolphine\Views;
class ViewAbstract
{
	protected $_params;

	public function __construct(){
		$this->_params = array();
	}

	/**
	 * @param field_type $_params
	 */
	public function setParam( $data, $value )
	{
		$this->_params[$data] = $value;
	}

	/**
	 * @return the $_params
	 */
	public function getParam( $data, $value = null )
	{
		if( isset($this->_params[$data]) )
		{
			return $this->_params[$data];
		} else {
			if(isset($value))
			{
				$this->setParam( $data, $value );
				return $value;
			}
			else {
				if(\System\Dolphine\Registry::getData( 'debugError' ) == true){
						return 'Parameter '.$data.' does not exist';
				} else {
						return false;
				}
			}
		}

	}

	public static function base_url( $url = null )
	{
		if(!isset($url)) $url = '';
		return \System\Dolphine\Registry::getSetting('base_url').$url;
	}

	public static function public_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('public_url').$image;
	}

	public static function local_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('local_url').$image;
	}

	public static function cms_url( $image = null )
	{
		if(!isset($image)) $image = '';
		return \System\Dolphine\Registry::getSetting('cmsbaseURL').$image;
	}

	public static function relative_path( $file = null )
	{
		return \System\Dolphine\Registry::getSetting('d_relative_path').$file;
	}

	public static function params( $name = null , $value = null )
	{
		$params = \System\Dolphine\Registry::getDeposit();
		if(isset( $name ))
		{
			if(isset( $params[$name] )){
				return $params[$name];
			} else {
				if(isset($value))
				{
					$params[$name] = $value;
					\System\Dolphine\Registry::setData( $name, $value );
					return $value;
				} else {
					if( \System\Dolphine\Registry::getData( 'debugError' ) == true){
						return 'Parameter '.$name.' does not exist';
					} else {
						return false;
					}
				}
			}
		}
		return $params;
	}

	public static function data( $name = null, $value = null )
	{
		return self::params( $name, $value );
	}

}