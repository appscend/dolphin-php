<?php
class formView extends genericView
{
	public function __construct( $gid ,  $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'fr');
		$this->addConfig('gid' , $gid );
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

}