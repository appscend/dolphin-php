<?php
class coverFlowView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'c');
	}

	public function setImageCornerRadius( $val, $valPad = null )
	{
		$this->addConfig('icr', $val, $valPad );
	}

	public function setImageDisplayMode( $val, $valPad = null )
	{
		$icm = self::checkIfIcmB( $val );
		$icmPad = self::checkIfIcmB( $valPad );
		$this->addConfig( 'icm', $icm, $icmPad );
	}

	public function setImageSpacing( $val, $valPad = null )
	{
		$this->addConfig( 'cs', $val, $valPad );
	}

	public function setImageAngle( $val, $valPad = null )
	{
		$this->addConfig( 'ca', $val, $valPad );
	}

	public function setFont()
	{
		$args = func_get_args();
		$no_args = sizeof($args);
		if( $no_args > 0 ) $this->addConfig( 'nf', $args[0], null );
		if( $no_args > 1 ) $this->addConfig( 'ns', $args[1], null );
		if( $no_args > 2 ) $this->addConfig( 'nc', $args[2], null );
		}

	}

	public function setNoLines( $val, $valPad = null )
	{
		$this->addConfig( 'nl', $val, $valPad );
	}

	public function setFontSize( $val, $valPad = null )
	{
		$this->addConfig( 'ns', $val, $valPad );
	}

	public function setFontColor( $val, $valPad = null )
	{
		$this->addConfig( 'nc', $val, $valPad );
	}

	public function setTextPadding( $val, $valPad = null )
	{
		$this->addConfig('np', $val, $valPad );
	}

	public function setTextShadowColor( $val, $valPad = null )
	{
		$this->addConfig('nsh', $val, $valPad );
	}

	public function setReflectionEnabled( $val, $valPad = null )
	{
		$this->addConfig('ref', $val, $valPad );
	}


	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

	private function checkIfIcmB( $val )
	{
		if(isset($val)) $val = strtolower($val);
		if(in_array($val, array( 'stf', 'aft', 'afl', 't', 'b', 'l', 'r', 'tl', 'tr', 'bl', 'br', 'c' )))
		{
			return $val;
		} else {
			return null;
		}
	}

}