<?php
class tabBarView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 't');
		$this->tabBar(true);
	}

	public function setTintColor( $val, $valPad = null )
	{
		$this->addConfig('tbc', $val, $valPad );
	}

	public function setMoreButtonBgColor( $val, $valPad = null )
	{
		$this->addConfig('mbc', $val, $valPad );
	}

	public function setMoreButtonTextColor( $val, $valPad = null )
	{
		$this->addConfig('mtc', $val, $valPad );
	}

	public function setMoreButtonTitle( $val, $valPad = null )
	{
		$this->addConfig('mtitle', $val, $valPad );
	}

	public function setMoreElementsBgColor( $val, $valPad = null )
	{
		$this->addConfig('ncc', $val, $valPad );
	}

	public function setMoreElementsAltBgColor( $val, $valPad = null )
	{
		$this->addConfig('acc', $val, $valPad );
	}

	public function setMoreElementsTextColor( $val, $valPad = null )
	{
		$this->addConfig('nc', $val, $valPad );
	}

	public function addTabs()
	{
		self::addSingleLevel( func_get_args(), '_tabs' );
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

}