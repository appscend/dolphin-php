<?php
class layarView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'lr');
	}

	public function setLayerName( $val, $valPad = null )
	{
		$this->addConfig('layer', $val, $valPad );
	}

	public function setConsumerKey( $val, $valPad = null )
	{
		$this->addConfig('key', $val, $valPad );
	}

	public function setConsumerSecret( $val, $valPad = null )
	{
		$this->addConfig('secret', $val, $valPad );
	}

	public function setVisibilityRadius( $val, $valPad = null )
	{
		$this->addConfig('radius', $val, $valPad );
	}

	public function useCustomInfo( $arg, $argPad = null )
	{
		$this->addConfig('cv', $arg, $argPad );
	}

	public function setCustomInfo( $content, $contentPad = null )
	{
		$this->addConfig('content', $content, $contentPad );
	}

	public function autoStartEnable( $arg, $argPad = null )
	{
		$this->addConfig('as', $arg, $argPad );
	}

	public function autoBackEnable( $arg, $argPad = null )
	{
		$this->addConfig('ap', $arg, $argPad );
	}

	public function generateView( $gzip = null, $cache = null )
	{
		parent::generateView( $gzip, $cache );
	}

}