<?php
class splitView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'l');
		$this->addConfig('padvt', 'split');
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}
}