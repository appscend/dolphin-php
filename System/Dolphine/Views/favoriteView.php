<?php
class favoriteView extends genericView
{
	public function __construct( $avi = null )
	{
		parent::__construct($avi);
		$this->addConfig('vt', 'fv');
	}

	public function addTemplate ( $param )
	{
		self::addMultileLevel( func_get_args(), '_elementTemplate' );
	}

	public function generateView( $gzip = null, $cache = null, $device_cache = null, $cache_time_out = null  )
	{
		parent::generateView( $gzip, $cache, $device_cache, $cache_time_out );
	}

}