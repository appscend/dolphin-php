<?php
class TabletView extends DView
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$view = new widgetView('main_view');
			$navigationBar = new navigationBar('DolphinPhp');
		$view->navigationBar($navigationBar);

		$label = new label('Welcome to dolphinPhp based App');
		$label->coords(0, 0, 640, 640, 'tablet portrait');
		$label->coords(0, 0, 640, 720, 'tablet landscape');
		
		$label->setFont(Config::font, 16);
		$label->setTextAlignment('center');

		$view->addElements( $label );
		$view->generateView();
	}
	
}