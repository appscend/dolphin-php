<?php
class Config
{
	
	/**
	 * from here you can add or alter everything
	 */	
	
	const appName =  'name of App';
	const font	  = 'HelveticaNeue';
	const fontB   = 'HelveticaNeue-Bold';
	const fontI   = 'Helvetica-Oblique';
	const pathImg = 'http://localhost/dolphinphp/Public/Assets/';

	const pWidth  = 320;
	const pHeight = 460;
	const lWidth = 460;
	const lHeight = 320;

	const xWidth  = 768;
	const xHeight = 1024;
	const xlWidth = 1024;
	const xlHeight = 768;
}