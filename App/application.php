<?php
/**
* don't alter or add new keys !!!!!!!
* edit only the values
*/
return array(
	// DEFAULT CONTROLLER
	'default_controller' => 'Phone',

	// APP_CODES
	'appKey' => 'appKey',
	'appId' => 'appId',
	'api_gzip_enabled' => true,

	// CONNECTIONS
	'base_url' => 'http://localhost/dolphin-php/',
	'public_url' => 'http://localhost/dolphin-php/Public/',
	'local_url' => 'http://localhost/dolphin-php/Public/',
	'cmsbaseURL' => 'https://s3-eu-west-1.amazonaws.com/assets.appcend.com/', //http://assets.appcend.com.s3.amazonaws.com/


	//DEBUG
	'debugError' => true,
	'gzip' => false,
	'cache' => false,
	'cache_time_out' => 900,
	'api_cache' => false,

	'master_api_cache' => "off", // pentru teste - off overrides everything, no cache at all
	'master_cache' => "off", // pentru teste - off overrides everything, no cache at all
);