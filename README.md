###################
# What is DolphinPHP
###################

DolphinPhp is an Application Development Framework - a toolkit - for people
who build mobile apps using Appscend's mobile platform. Its goal is to enable
you to develop mobile apps much faster than you could if you were writing code
from scratch, by providing a rich set of helpers for commonly needed tasks,
as well as a simple and logical structure to access these helpers.
DolphinPhp lets you focus on your project by minimizing the amount of code
needed for developing apps in Appscend's mobile platform.

*******************
# Release Information
*******************

This repo contains in development code for future releases.

*******************
# Server Requirements
*******************

-  PHP version 5.3.0 or newer.

*******************
# Framework Version
*******************

-  0.67
