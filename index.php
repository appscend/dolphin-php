<?php
error_reporting(0);
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
defined('BASEPATH') or define('BASEPATH', dirname(realpath(__FILE__)). DS  );
defined('COREPATH') or define('COREPATH', BASEPATH . 'System'.DS.'Dolphine'. DS);
defined('PKGPATH')  or define('PKGPATH', BASEPATH . 'System'.DS.'Packages'.DS);
defined('APPPATH')  or define('APPPATH', BASEPATH . 'App'.DS);
defined('CONFPATH') or define('CONFPATH', APPPATH . 'Config'.DS);
defined('VIEWCORE') or define('VIEWCORE', COREPATH. 'Views'.DS);
defined('VIEWELEMENTS') or define('VIEWELEMENTS', COREPATH. 'ViewElements'.DS);
defined('VIEWACTIONS') or define('VIEWACTIONS', COREPATH. 'ViewActions'.DS);
defined('LOGDIR') or define('LOGDIR', dirname(realpath(__FILE__)). DS.'Public' );
ob_start();
if( ! function_exists('bootstrap_autoloader') )
{
	function bootstrap_autoloader()
	{
		require(COREPATH . 'Autoloader.php');
		System\Dolphine\Autoloader::setIncludePath(COREPATH);
		System\Dolphine\Autoloader::setIncludePaths(array(
			COREPATH,
			CONFPATH,
			VIEWCORE,
			VIEWELEMENTS,
			VIEWACTIONS,
			APPPATH . 'Controllers/',
			APPPATH . 'Views/',
			APPPATH . 'Helpers/',
			APPPATH . 'Models/',
			));
		System\Dolphine\Autoloader::register();

		System\Dolphine\Autoloader::add_classes(array(
				"System\\Dolphine\\Helper\\User" => COREPATH . 'Helper'.DS.'User.php',
				"System\\Dolphine\\Helper\\viewHelper" => COREPATH . 'Helper'.DS.'viewHelper.php',
				"System\\Dolphine\\Struct" => COREPATH . 'Struct.php',
				"System\\Dolphine\\Controller\\ControllerAbstract" => COREPATH . 'Controller'.DS.'ControllerAbstract.php',
				"System\\Dolphine\\Helper\\HelperAbstract" => COREPATH . 'Helper'.DS.'HelperAbstract.php',
				"System\\Dolphine\\Views\\ViewAbstract" => VIEWCORE . 'ViewAbstract.php',
				"System\\Dolphine\\Model\\ModelAbstract" => COREPATH. 'Model'.DS.'ModelAbstract.php',
				"System\\Dolphine\\Registry" => COREPATH . 'Registry.php',
				"System\\Dolphine\\imGen" => COREPATH . 'imGen.php',
				"System\\Dolphine\\Controller\\ControllerDefinition" => COREPATH . 'Controller'.DS.'ControllerDefinition.php',
				"System\\Dolphine\\Helper\\Act" => COREPATH . 'Helper'.DS.'Act.php',
				"System\\Dolphine\\Helper\\API" => COREPATH . 'Helper'.DS.'API.php',
			));

		alias(array(
			"System\\Dolphine\\Helper\\viewHelper" => 'viewHelper',
			"System\\Dolphine\\Helper\\User" => 'User',
			"System\\Dolphine\\Autoloader" => 'Autoloader',
			"System\\Dolphine\\Struct" => 'Struct',
			"System\\Dolphine\\Controller\\ControllerAbstract" => 'DController',
			"System\\Dolphine\\Views\\ViewAbstract" => 'DView',
			"System\\Dolphine\\Helper\\HelperAbstract" => 'DHelper',
			"System\\Dolphine\\Model\\ModelAbstract" => 'DModel',
			"System\\Dolphine\\Registry" => 'Registry',
			"System\\Dolphine\\imGen" => 'imGen',
			"System\\Dolphine\\Controller\\ControllerDefinition" => 'ControllerDefinition',
			"System\\Dolphine\\Helper\\Act" => 'Act',
			"System\\Dolphine\\Helper\\API" => 'API',
			));
	}
}

if( ! function_exists('alias') )
{
	function alias($array)
	{
		foreach($array as $class => $alias)
		{
			class_alias($class, $alias);
		}
	}
}

////
//	Initialize Autoloader
///////////
bootstrap_autoloader();

$_configs = require APPPATH . 'application.php';
foreach ($_configs as $key => $value) {
	Registry::setSetting($key, $value);
}

Registry::setSetting( 'd_relative_path', BASEPATH.'Public'.DS );
Registry::setSetting( 'api_original', 'no' );

$show_errors = Registry::getSetting('debugError');

if( $show_errors === true ){
	DolphinError::init();
	set_error_handler( "DolphinError::customError" );
	set_exception_handler( "DolphinError::customexceptionError" );
	register_shutdown_function( "DolphinError::handleShutdown" );
	unset($show_errors);
}

API::init();
API::setConf(
	Registry::getSetting('appId'),
	Registry::getSetting('appKey'),
	Registry::getSetting('api_gzip_enabled')
);
if( Registry::getSetting('master_api_cache') != 'off'){
	ApiCache::init();
}
if( Registry::getSetting('master_cache') != 'off'){
	DCache::init();
}
Router::init();
